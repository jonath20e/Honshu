/* fenetre.c */
#include <SDL.h>
#include <SDL_image.h>

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

#include "layer.h"
#include "list.h"
#include "map.h"
#include "solveur.h"
#include "rules.h"

#define TILESET "images/tileset_s.bmp"
#define TILESET_S "images/tileset_xs.bmp"
#define BACKGROUND "images/old_map.jpg"
#define PAPER "images/old_paper.png"
#define WOOD "images/wood.jpg"
#define NUMBERS "images/numbers.bmp"
#define RULES "images/rules.bmp"
#define SCORE "images/score.bmp"
#define WAIT "images/wait.bmp"
#define CHRONO "images/chrono.bmp"

#define TILESIZE 50
#define TILESIZE_S 25

#define DRAWUI "SDL_RenderCopy( gRenderer, texture_bg, NULL, &destR); drawMapSDLT(gRenderer,texture,tileset,mox,moy,m); SDL_RenderCopy( gRenderer, wood, NULL, &listrect); drawListSDLT(gRenderer,texture_s, paper, tileset_s,lox,loy,l); SDL_RenderCopy( gRenderer, wood, NULL, &rightui ); drawCardSDLT(gRenderer,texture, paper, tileset, rightui.x,rightui.y,c); drawScore(gRenderer,numbers,score,numbersR,leftui,countPoints(m))"


void initTexture(SDL_Rect* tileset, int size)
{
    int index = 1;
    int i = 0;
    int j = 0;
    
    tileset[0].x = 0;
    tileset[0].y = 0;
    tileset[0].w = 0;
    tileset[0].h = 0;
    for(i = 0; i<2; i++){
        for(j = 0; j<3; j++){
            printf("index: %d\n",index);
            tileset[index].x = i * size;
            tileset[index].y = j * size;
            tileset[index].w = size;
            tileset[index].h = size;
            index++;
        }
    }
}

void drawMapSDLT(SDL_Renderer* gRenderer, SDL_Texture* texture, SDL_Rect* tileset, int x_o, int y_o, Map m)
{
    int x = 0;
    int y = 0;
    SDL_Rect renderQuad;
    Ground g = VIDE;
    
    /* clear screen */
    /*SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );
     SDL_RenderClear( gRenderer );*/
    
    for(x = 0; x < getSize(m); x++){
        for(y = 0; y < getSize(m); y++){
            
            g = readMap(m,x,y);
            if(g != VIDE) {
                /*printf("rendering x:%d y:%d w:%d h:%d card:%d\n",x,y,tileset[g].w,tileset[g].h,g);
                printf("tileset values: x: %d y: %d w: %d h:%d: \n",tileset[g].x,tileset[g].y,tileset[g].w,tileset[g].h);*/
                renderQuad.x = (tileset[g].w * x) + x_o;
                renderQuad.y = (tileset[g].h * y) + y_o;
            }
            
            renderQuad.w = tileset[g].w;
            renderQuad.h = tileset[g].h;
            
            /*Render to screen*/
            SDL_RenderCopy( gRenderer, texture, &tileset[g], &renderQuad );
        }
    }
    }

void drawCardSDLT(SDL_Renderer* gRenderer, SDL_Texture* texture, SDL_Texture* paper, SDL_Rect* tileset, int x_o, int y_o, Card c)
{
    int x = 0;
    int y = 0;
    SDL_Rect renderQuad;
    SDL_Rect paperQuad;
    Ground g = VIDE;
    paperQuad.x = x_o - 3;
    paperQuad.y = y_o - 3;
    /* clear screen */
    /*SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );
    SDL_RenderClear( gRenderer );*/
    if(readRot(c) == TOP || readRot(c) == BOTTOM){
        paperQuad.w = tileset[1].w * 2 + 6;
        paperQuad.h = tileset[1].w * 3 + 6;
        SDL_RenderCopy( gRenderer, paper, NULL, &paperQuad );


	    for(x = 0; x < 2; x++){
		    for(y = 0; y < 3; y++){
		    
			if(readRot(c) == TOP){
			    g = readCardM(c,x,y);
			}
			else if(readRot(c) == BOTTOM){
			    g = readCard(c,5 - (3 * x + y));
			}
			/*printf("rendering x:%d y:%d w:%d h:%d card:%d\n",x,y,tileset[g].w,tileset[g].h,g);
			printf("tileset values: x: %d y: %d w: %d h:%d: \n",tileset[g].x,tileset[g].y,tileset[g].w,tileset[g].h);*/
			renderQuad.x = (tileset[g].w * x) + x_o;
			renderQuad.y = (tileset[g].h * y) + y_o;
			renderQuad.w = tileset[g].w;
			renderQuad.h = tileset[g].h;

			/*Render to screen*/
			SDL_RenderCopy( gRenderer, texture, &tileset[g], &renderQuad );
		    }
	    }
    }
    else{
        paperQuad.w = tileset[1].w * 3 + 6;
        paperQuad.h = tileset[1].w * 2 + 6;
        SDL_RenderCopy( gRenderer, paper, NULL, &paperQuad );

        for(y = 0; y < 2; y++){
            for(x = 0; x < 3; x++){
                
                if(readRot(c) == RIGHT){
                    g = readCard(c,(3 * y) + (2-x));
                }
                else if(readRot(c) == LEFT){
                    g = readCard(c,3 - (3 * y) + x);
                }
                
                /*printf("rendering x:%d y:%d w:%d h:%d card:%d\n",x,y,tileset[g].w,tileset[g].h,g);
                printf("tileset values: x: %d y: %d w: %d h:%d: \n",tileset[g].x,tileset[g].y,tileset[g].w,tileset[g].h);*/
                renderQuad.x = (tileset[g].w * x) + x_o;
                renderQuad.y = (tileset[g].h * y) + y_o;
                
                renderQuad.w = tileset[g].w;
                renderQuad.h = tileset[g].h;

                /*Render to screen*/
                SDL_RenderCopy( gRenderer, texture, &tileset[g], &renderQuad );
            }
	    }

    }
    
    /*Update screen*/
}

void drawListSDLT(SDL_Renderer* gRenderer, SDL_Texture* texture, SDL_Texture* paper, SDL_Rect* tileset, int x_o, int y_o, List l)
{
    Element e = readFirst(l);
    int x = x_o;
    while(e != 0)
    {
        drawCardSDLT(gRenderer,texture, paper, tileset, x_o + x,y_o,readCardE(e));
        x = 8 + x + (TILESIZE_S * 2);
        e = skipCard(e);
    }
}

int selectedCard(int x_o, int y_o, List l, int click_x, int click_y)
{
  

     if(click_y < y_o || click_y > y_o + 3 * TILESIZE_S)
        return -1;

    int x = x_o + 18;
    int i = 0;
    for(i = 0; i < getListSize(l); i++)
    {
        if(click_x > x && click_x < x + TILESIZE_S * 2)
            return readCardId(findCardPos(l,i));
        x = 8 + x + (TILESIZE_S * 2);
    }
    return -1;
}

void fillTileset(SDL_Rect* tileset, int tsize)
{
    int i = 0, j = 0;
    int index = 1;
    tileset[0].x = 0;
    tileset[0].y = 0;
    tileset[0].w = 0;
    tileset[0].h = 0;
    for(i = 0; i<2; i++){
        for(j = 0; j<3; j++){
            tileset[index].x = i * tsize;
            tileset[index].y = j * tsize;
            tileset[index].w = tsize;
            tileset[index].h = tsize;
            index++;
        }
    }
}

void drawErrorRect(SDL_Renderer* gRenderer,int tilesize,int offsetx,int offsety, Rotation orientation)
{

	SDL_Rect rect = {.x = offsetx, .y = offsety, .h = 2*tilesize, .w = 2*tilesize};
	if(orientation == LEFT || orientation == RIGHT)
	{
		rect.w = rect.w + tilesize;
	}
	else
	{
		rect.h = rect.h + tilesize;
	}
	SDL_SetRenderDrawColor(gRenderer,0xAA,0x00,0x00,0xFF);

	SDL_RenderPresent(gRenderer);
	SDL_RenderDrawRect(gRenderer,&rect);
	SDL_RenderPresent(gRenderer);
}

void drawTime(SDL_Renderer* gRenderer,SDL_Texture* numbers,SDL_Texture* chrono,SDL_Rect* numbersR,SDL_Rect leftui,double time)
{
	long int ct = (long int)time;
	int length = 0;
	while(ct != 0)
	{
		length++;
		ct = ct/10;
	}
	ct = time;
    printf("time: %ld",ct);
    int decimal = ct % 10;;
    SDL_Rect renderQuad = { .x = leftui.x, .y = 20 + leftui.y, .h = 20, .w = 70} ;
    SDL_RenderCopy( gRenderer, chrono, NULL, &renderQuad );
    while(ct != 0 && length >= 0)
    {
        printf("selecting %d",decimal);
        renderQuad.x = (length * numbersR[decimal].w  ) + leftui.x + 40;
        renderQuad.y = 20 + leftui.y;
        renderQuad.w = numbersR[decimal].w;
        renderQuad.h = numbersR[decimal].h;
        /*printf("tileset values: x: %d y: %d w: %d h:%d: \n",numbersR[decimal].x,numbersR[decimal].y,numbersR[decimal].w,numbersR[decimal].h);*/
        SDL_RenderCopy( gRenderer, numbers, &numbersR[decimal], &renderQuad );
        ct = ct / 10;
        decimal = ct % 10;
        length--;
    }
}

void drawScore(SDL_Renderer* gRenderer,SDL_Texture* numbers,SDL_Texture* sui,SDL_Rect* numbersR,SDL_Rect leftui,int score)
{
    int i = 2;
    int decimal = score % 10;;
    SDL_Rect renderQuad = { .x = leftui.x, .y = leftui.y, .h = 20, .w = 70} ;
    SDL_RenderCopy( gRenderer, sui, NULL, &renderQuad );
    while(score != 0 && i >= 0)
    {
        renderQuad.x = i * numbersR[decimal].w + leftui.x + 40;
        renderQuad.y = leftui.y;
        renderQuad.w = numbersR[decimal].w;
        renderQuad.h = numbersR[decimal].h;
        /*printf("tileset values: x: %d y: %d w: %d h:%d: \n",numbersR[decimal].x,numbersR[decimal].y,numbersR[decimal].w,numbersR[decimal].h);*/
        SDL_RenderCopy( gRenderer, numbers, &numbersR[decimal], &renderQuad );
        score = score / 10;
        decimal = score % 10;
        i--;
    }
}


int main(int argc, char** argv){
	srand(time(NULL));

	SDL_Window* win = NULL;
	SDL_Surface* screen = NULL;
	SDL_Event e;
    SDL_Rect tileset[7];
    SDL_Rect tileset_s[7];
    SDL_Rect numbersR[10];
    int i = 0;
    for(i=0;i<10;i++){
        numbersR[i].x = 0;
        numbersR[i].y = i * 20;
        numbersR[i].h = 20;
        numbersR[i].w = 20;
    }
    fillTileset(tileset, TILESIZE);
    fillTileset(tileset_s, TILESIZE_S);


    SDL_Texture* texture = NULL;
    SDL_Texture* paper = NULL;
    SDL_Texture* texture_s = NULL;
    SDL_Texture* texture_bg = NULL;
    SDL_Texture* wood = NULL;
    SDL_Texture* numbers = NULL;
    SDL_Texture* rules = NULL;
    SDL_Texture* score = NULL;
    SDL_Texture* wait = NULL;
    SDL_Texture* chrono = NULL;

    SDL_Renderer* gRenderer = NULL;
    
    int togglemenu = 0;
    int index_i = 0;
    int rshift = 0;
    int mapsize = 9;
    int refresh = 0;
    int start = 0, end = 0;
	Point_S ps = { .l = 0, .p = 0, .r = 0, .v = 0, .f = 0};
	int quit = 0;
    int mx, my, index_x = 0, index_y = 0, id = 0;
    /* map offset */
    int mox = TILESIZE * 3, moy = TILESIZE_S;
    /* taille de l'écran */
    int screenx = TILESIZE * (mapsize + 6);
    int screeny = TILESIZE * (mapsize + 3) + TILESIZE_S;
    /* list offset */
    int lox = 18;
    int loy = screeny - TILESIZE_S * 4;
    
    SDL_Rect rightui = { .x = screenx - mox, .y = loy - TILESIZE_S - mox, .w = mox, .h = mox};
    SDL_Rect leftui = { .x = 20, .y = 100, .w = mox, .h = screeny - 5 };
    
    SDL_Rect listrect = { .x = 0, .y = loy - TILESIZE_S, .w = screenx, .h = screenx - loy };
    SDL_Rect destR = { .x = 0, .y = 0, .w = screenx, .h = screeny };
    SDL_Rect waitR = { .x = screenx/2 - 100, .y = screeny/2 - 100, .w = 230, .h = 230 };

    Card c = NULL;
	List l = NULL;

	SDL_RendererInfo info;
	int r = SDL_GetNumRenderDrivers();
	while ( index_i < r )
	{
		if ( SDL_GetRenderDriverInfo(index_i,&info) == 0 )
		{
			printf("%s\n",info.name);
		}
		index_i++;
	}

	if(SDL_Init(SDL_INIT_VIDEO) < 0){
		printf("Init error: %s\n", SDL_GetError());
        return EXIT_FAILURE;
	}
    
    /*Set texture filtering to linear*/
    if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
        printf( "Warning: Linear texture filtering not enabled!" );
    
    win = SDL_CreateWindow("Honshu",SDL_WINDOWPOS_UNDEFINED,SDL_WINDOWPOS_UNDEFINED,screenx,screeny, SDL_WINDOW_SHOWN);
    
    if(win == NULL){
        printf("win error: %s\n",SDL_GetError());
        return EXIT_FAILURE;
    }
 
    /*screen = SDL_GetWindowSurface(win);
    SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format,0xee,0xFF,0xFF));
    SDL_UpdateWindowSurface(win);*/
    
    /*Create renderer for window*/
    gRenderer = SDL_CreateRenderer(win, 0, SDL_RENDERER_ACCELERATED);
    if( gRenderer == NULL )
    {
        printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
    }
    else
    {
        /*Initialize renderer color*/
        SDL_SetRenderDrawColor( gRenderer, 0xdd, 0xFF, 0xFF, 0xFF );
       
    }
    
    /* Init carte et list */
    if(argc == 2){
        l = initCardFile(argv[1],l);
        if(l == NULL){
            printf("Impossible de charger le fichier\n");
            l = initCardPack(12);
        }
	else{
		printf("%s chargé.\n",argv[1]);
	}
    }
    else{
        l = initCardPack(12);
    }
    Map m = initMap(mapsize);

    screen = SDL_LoadBMP(TILESET);
    if(screen == NULL){
        printf("loading %s failed! %s\n",TILESET,SDL_GetError());
    }
    else{
        texture = SDL_CreateTextureFromSurface(gRenderer, screen);
    }
    SDL_FreeSurface(screen); /* we got the texture now -> free surface */
    screen = SDL_LoadBMP(TILESET_S);
    if(screen == NULL){
        printf("loading %s failed! %s\n",TILESET_S,SDL_GetError());
    }
    else{
        texture_s = SDL_CreateTextureFromSurface(gRenderer, screen);
    }
    SDL_FreeSurface(screen); /* we got the texture now -> free surface */
    screen = SDL_LoadBMP(WAIT);
    if(screen == NULL){
        printf("loading %s failed! %s\n",WAIT,SDL_GetError());
    }
    else{
        wait = SDL_CreateTextureFromSurface(gRenderer, screen);
    }
    SDL_FreeSurface(screen); /* we got the texture now -> free surface */
    screen = SDL_LoadBMP(CHRONO);
    if(screen == NULL){
        printf("loading %s failed! %s\n",CHRONO,SDL_GetError());
    }
    else{
        chrono = SDL_CreateTextureFromSurface(gRenderer, screen);
    }


    int flags=IMG_INIT_JPG|IMG_INIT_PNG;
    int initted=IMG_Init(flags);
    if((initted&flags) != flags) {
        printf("IMG_Init: Failed to init required jpg and png support!\n");
        printf("IMG_Init: %s\n", IMG_GetError());
    }
    else
    {
        SDL_FreeSurface(screen); /* we got the texture now -> free surface */
        screen = IMG_Load(BACKGROUND);
        if(screen == NULL){
            printf("loading %s failed! %s\n",TILESET,SDL_GetError());
        }
        else{
            
            texture_bg = SDL_CreateTextureFromSurface(gRenderer, screen);
        }
        SDL_FreeSurface(screen); /* we got the texture now -> free surface */
        screen = IMG_Load(PAPER);
        if(screen == NULL){
            printf("loading %s failed! %s\n",PAPER,SDL_GetError());
            screen = IMG_Load("images/old_paper.jpg");
            paper = SDL_CreateTextureFromSurface(gRenderer, screen);
        }
        else{
            paper = SDL_CreateTextureFromSurface(gRenderer, screen);
        }
        SDL_FreeSurface(screen); /* we got the texture now -> free surface */
        screen = IMG_Load(WOOD);
        if(screen == NULL){
            printf("loading %s failed! %s\n",WOOD,SDL_GetError());
        }
        else {
            wood = SDL_CreateTextureFromSurface(gRenderer, screen);
        }
        SDL_FreeSurface(screen); /* we got the texture now -> free surface */
        screen = IMG_Load(NUMBERS);
        if(screen == NULL){
            printf("loading %s failed! %s\n",NUMBERS,SDL_GetError());
        }
        else {
            numbers = SDL_CreateTextureFromSurface(gRenderer, screen);
        }
        SDL_FreeSurface(screen); /* we got the texture now -> free surface */
        screen = IMG_Load(RULES);
        if(screen == NULL){
            printf("loading %s failed! %s\n",RULES,SDL_GetError());
        }
        else {
            rules = SDL_CreateTextureFromSurface(gRenderer, screen);
        }
        SDL_FreeSurface(screen); /* we got the texture now -> free surface */
        screen = IMG_Load(SCORE);
        if(screen == NULL){
            printf("loading %s failed! %s\n",SCORE,SDL_GetError());
        }
        else {
            score = SDL_CreateTextureFromSurface(gRenderer, screen);
        }
    }
    
    /*SDL_SetRenderDrawColor(gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);*/
    SDL_RenderClear(gRenderer);

    /*SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format,0xee,0xFF,0xFF));*/
    SDL_UpdateWindowSurface(win);
    SDL_RenderClear(gRenderer);
    SDL_RenderCopy( gRenderer, rules, NULL, NULL);

    c = readCardE(readFirst(l));
    
    SDL_RenderPresent( gRenderer );
    
    
    /*SDL_RenderPresent( gRenderer );*/
    fflush(stdout);
    /*SDL_UpdateWindowSurface(win);*/
    while(!quit){
        while(SDL_PollEvent(&e) != 0){
            if(/*e->type == SDL_MOUSEMOTION || e->type == SDL_MOUSEBUTTONUP  || */ e.type == SDL_MOUSEBUTTONDOWN)
            {
                /*Get mouse position*/
                SDL_GetMouseState( &mx, &my );
                /*printf("officially: x: %d y: %d\n",mx,my);*/
                index_x = (int)floor((float)(mx-mox)/(float)TILESIZE);
                index_y = (int)floor((float)(my-moy)/(float)TILESIZE);
                /*printf("index: x: %d y: %d\n",index_x,index_y);*/
                
                if(c != NULL)
                {
		    if(index_x >= 0  && index_x < mapsize && index_y >= 0 && index_y < mapsize)
		    {
			    setPos(c,index_x,index_y);
			    if(respecteRegles(m,c))
			    {
				SDL_RenderClear( gRenderer );
				SDL_RenderCopy( gRenderer, texture_bg, NULL, &destR);
				drawMapSDLT(gRenderer,texture,tileset,mox,moy,m);
				insertCard(m,c,index_x,index_y);
				appendHeadList(getMapList(m),popId(l,readCardId(c)));
				drawCardSDLT(gRenderer,texture, paper, tileset, (index_x * TILESIZE) + mox,(index_y * TILESIZE) + moy,c);
				drawScore(gRenderer,numbers,score,numbersR,leftui,countPoints(m,ps));
				if(listIsEmpty(l))
				{
				    c = NULL;
				    printf("List vide\n");
				}
				else
				{
				    c = readCardE(readFirst(l));
				    SDL_RenderCopy( gRenderer, wood, NULL, &rightui );
				    drawCardSDLT(gRenderer,texture, paper, tileset, rightui.x,rightui.y,c);
				}
				SDL_RenderCopy( gRenderer, wood, NULL, &listrect);
				drawListSDLT(gRenderer,texture_s, paper, tileset_s,lox,loy,l);
				
				SDL_RenderPresent( gRenderer );

			    }
			    else
			    {
				printf("Placement non autorisé\n");
				drawErrorRect(gRenderer,TILESIZE,index_x*TILESIZE+mox,index_y*TILESIZE+moy,readRot(c));
			    }
		    }
                    else{
                        id = selectedCard(lox,loy,l,mx,my);
                        if(id == -1)
                            printf("Pas une selection \n");
                        else {
                            changeRotation(c,TOP);
                            printf("Carte selectionné: %d \n",id);
                            c = findCard(l,id);
                            SDL_RenderClear( gRenderer );
                            SDL_RenderCopy( gRenderer, texture_bg, NULL, &destR);
                            drawMapSDLT(gRenderer,texture,tileset,mox,moy,m);
                            SDL_RenderCopy( gRenderer, wood, NULL, &listrect);
                            drawListSDLT(gRenderer,texture_s, paper, tileset_s,lox,loy,l);
                            SDL_RenderCopy( gRenderer, wood, NULL, &rightui );
                            drawCardSDLT(gRenderer,texture, paper, tileset, rightui.x,rightui.y,c);
                            drawScore(gRenderer,numbers,score,numbersR,leftui,countPoints(m,ps));
                            SDL_RenderPresent( gRenderer );

                        }
                    }
                }
                else
                {
                    printf("List vide\n");
                }
            }
            else if( e.type == SDL_KEYDOWN )
            {
                if(e.key.keysym.sym == SDLK_h){
                    SDL_RenderClear(gRenderer);
                    if(togglemenu) {
                        SDL_RenderCopy( gRenderer, rules, NULL, NULL);
                    }
                    else {
                        SDL_RenderCopy( gRenderer, texture_bg, NULL, &destR);
                        drawMapSDLT(gRenderer,texture,tileset,mox,moy,m);
                        SDL_RenderCopy( gRenderer, wood, NULL, &listrect);
                        drawListSDLT(gRenderer,texture_s, paper, tileset_s,lox,loy,l);
                        SDL_RenderCopy( gRenderer, wood, NULL, &rightui );
                        if(c != NULL)
				drawCardSDLT(gRenderer,texture, paper, tileset, rightui.x,rightui.y,c);
                        drawScore(gRenderer,numbers,score,numbersR,leftui,countPoints(m,ps));
                    }
                    SDL_RenderPresent(gRenderer);
                    togglemenu = !togglemenu;
                }
                else if(e.key.keysym.sym == SDLK_c){
                    SDL_RenderClear(gRenderer);
                }
                else if(e.key.keysym.sym == SDLK_u){
                    SDL_UpdateWindowSurface(win);
                }
                else if(e.key.keysym.sym == SDLK_q){
                    SDL_UpdateWindowSurface(win);
                }
                else if(e.key.keysym.sym == SDLK_r){
                    if(c != NULL){
                        changeRotation(c, (readRot(c) + 1)%4);
                        /*printf("Rotation changé a : %d\n",readRot(c));*/
                        SDL_RenderClear( gRenderer );
                        SDL_RenderCopy( gRenderer, texture_bg, NULL, &destR);
                        drawMapSDLT(gRenderer,texture,tileset,mox,moy,m);
                        SDL_RenderCopy( gRenderer, wood, NULL, &listrect);
                        drawListSDLT(gRenderer,texture_s, paper, tileset_s,lox,loy,l);
                        SDL_RenderCopy( gRenderer, wood, NULL, &rightui );
                        drawCardSDLT(gRenderer,texture, paper, tileset, rightui.x,rightui.y,c);
                        drawScore(gRenderer,numbers,score,numbersR,leftui,countPoints(m,ps));
                        SDL_RenderPresent( gRenderer );
                    }
                }
                else if(e.key.keysym.sym == SDLK_e){
                    c = retirerCard(m);
                    if(c == NULL){
                        c = readCardE(readFirst(l));
                        printf("Impossible d'enlever une carte : liste vide\n");
                    }
                    else
                    {
                        l = appendHeadList(l,c);
                        /*printf("Grille ---> Tuiles \n");*/
                        displayList(getMapList(m));
                        SDL_RenderClear( gRenderer );
                        SDL_RenderCopy( gRenderer, texture_bg, NULL, &destR);
                        drawMapSDLT(gRenderer,texture,tileset,mox,moy,m);
                        SDL_RenderCopy( gRenderer, wood, NULL, &listrect);
                        drawListSDLT(gRenderer,texture_s, paper, tileset_s,lox,loy,l);
                        SDL_RenderCopy( gRenderer, wood, NULL, &rightui );
                        drawCardSDLT(gRenderer,texture, paper, tileset, rightui.x,rightui.y,c);
                        drawScore(gRenderer,numbers,score,numbersR,leftui,countPoints(m,ps));
                        SDL_RenderPresent( gRenderer );
                    }
                }
		else if(e.key.keysym.sym == SDLK_RSHIFT){
			rshift = 1;
		}
		else if(e.key.keysym.sym == SDLK_1 && rshift){
			printf("solving\n");
			start = clock();
                        SDL_RenderPresent( gRenderer );
                        SDL_RenderCopy( gRenderer, wait, NULL, &waitR );
                        SDL_RenderPresent( gRenderer );
			l = iterate(m,l,ps);
			end = clock();
			refresh = 1;
		}
		else if(e.key.keysym.sym == SDLK_2 && rshift){
			printf("solving\n");
			start = clock();
                        SDL_RenderPresent( gRenderer );
                        SDL_RenderCopy( gRenderer, wait, NULL, &waitR );
                        SDL_RenderPresent( gRenderer );
			solveur_foret(m,l,0,ps);
			end = clock();
			refresh = 1;
		}else if(e.key.keysym.sym == SDLK_3 && rshift){
			printf("solving\n");
			start = clock();
                        SDL_RenderPresent( gRenderer );
                        SDL_RenderCopy( gRenderer, wait, NULL, &waitR );
                        SDL_RenderPresent( gRenderer );
			solveur_distance(m,l,ps);
			end = clock();
			refresh = 1;
		}else if(e.key.keysym.sym == SDLK_4 && rshift){
			printf("solving\n");
			start = clock();
                        SDL_RenderPresent( gRenderer );
                        SDL_RenderCopy( gRenderer, wait, NULL, &waitR );
                        SDL_RenderPresent( gRenderer );
			solveur_ver1(m,l,ps);
			end = clock();
			refresh = 1;
		}else if(e.key.keysym.sym == SDLK_5 && rshift){
			printf("solving\n");
			start = clock();
                        SDL_RenderPresent( gRenderer );
                        SDL_RenderCopy( gRenderer, wait, NULL, &waitR );
                        SDL_RenderPresent( gRenderer );
			solveur_recuit(m,l,ps);
			end = clock();
			refresh = 1;
		}

		if(refresh)
		{
			printf("Le solveur a pris %lf secondes \n",((double)end-start)/CLOCKS_PER_SEC);
			SDL_RenderClear( gRenderer );
                        SDL_RenderCopy( gRenderer, texture_bg, NULL, &destR);
                        drawMapSDLT(gRenderer,texture,tileset,mox,moy,m);
                        SDL_RenderCopy( gRenderer, wood, NULL, &listrect);
                        drawListSDLT(gRenderer,texture_s, paper, tileset_s,lox,loy,l);
                        SDL_RenderCopy( gRenderer, wood, NULL, &rightui );
                        drawCardSDLT(gRenderer,texture, paper, tileset, rightui.x,rightui.y,c);
                        drawScore(gRenderer,numbers,score,numbersR,leftui,countPoints(m,ps));
                        drawTime(gRenderer,numbers,chrono,numbersR,leftui,((double)end-start));
                        SDL_RenderPresent( gRenderer );
			
			refresh = 0;
		}
            }
	    else if(e.type == SDL_KEYUP){
		if(e.key.keysym.sym == SDLK_RSHIFT){
			rshift = 0;
		}
	    }
            else if(e.type == SDL_QUIT){
                quit = 1;
            }

        }
    }

    /*Free loaded images*/
    if( texture != NULL )
    {
        SDL_DestroyTexture( texture );
        texture = NULL;
    }
    if( texture_s != NULL )
    {
        SDL_DestroyTexture( texture_s );
        texture_s = NULL;
    }
    if( texture_bg != NULL )
    {
        SDL_DestroyTexture( texture_bg );
        texture_bg = NULL;
    }
    if( paper != NULL )
    {
        SDL_DestroyTexture( paper );
        paper = NULL;
    }
    if( wood != NULL )
    {
        SDL_DestroyTexture(wood);
        wood = NULL;
    }
    if( numbers != NULL )
    {
        SDL_DestroyTexture(numbers);
        numbers = NULL;
    }
    if( rules != NULL )
    {
        SDL_DestroyTexture(rules);
        rules = NULL;
    }
    if( score != NULL )
    {
        SDL_DestroyTexture(score);
        score = NULL;
    }
    if( wait != NULL )
    {
        SDL_DestroyTexture(wait);
        wait = NULL;
    }
    if( chrono != NULL )
    {
        SDL_DestroyTexture(chrono);
        chrono = NULL;
    }
    if(screen != NULL){
        SDL_FreeSurface(screen); /* we got the texture now -> free surface */
        screen = NULL;
    }
    SDL_DestroyRenderer( gRenderer );	
    win = NULL;
    gRenderer = NULL;
    
    /*Quit SDL subsystems*/
    IMG_Quit();
    SDL_Quit();
    SDL_DestroyWindow(win);
    SDL_Quit();
    
    freeList(l);
    freeMap(m);
    
	return EXIT_SUCCESS;
}



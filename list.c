#include <stdlib.h>
#include <stdio.h>

#include "list.h"
#include "layer.h"

struct Element_s {
	Card card;
	Element next;
};

struct List_s {
	Element first;
};

List createList() {
	List l = (List) malloc(sizeof(struct List_s));
	l->first = NULL;

	return l;
}

List flipList(List l)
{
	Element f = l->first;
	Element n = f->next;
	Element next = NULL;
	f->next = NULL;
	Element newlist = f;
	Element head = n;
	while(n != NULL)
	{
		head = n;
		next = n->next;
		head->next = newlist;
		n = next;
		newlist = head;
	}
	l->first = newlist;
	return l;
}


int getListSize(List l) {
  Element s = l->first;
  int size = 0;

  while (s != NULL) {
    size++;

    s = s->next;
  }

  return size;
}

int listIsEmpty(List l) { return l->first == NULL; }

void freeList(List l) {
	Element m = l->first;
	Element n = l->first;

	while (m != NULL) {
		m = m->next;
		free(n->card);
		free(n);
		n = m;
	}
	free(l);
}

List appendHeadList(List l, Card card) {
	Element elt = (Element) malloc(sizeof(struct Element_s));

	elt->card = card;
	elt->next = l->first;

	l->first = elt;
	return l;
}

List appendTailList(List l, Card card)
{
	l = flipList(l);
	if(readCardId(readCardE(l->first)))
	{
		Card c = popFirstCard(l);
		l = appendHeadList(l,card);
		l = appendHeadList(l,c);
	}
	else
	{
		l = appendHeadList(l,card);
	}
	l = flipList(l);
	return l;
}

List appendHeadListE(List l, Element e) {
	/*e->next = l->first;*/

	l->first = e;

	return l;
}

Card findCard(List pack, int id) {
  	Card card;
	Element elt = pack->first;

	while (elt != NULL) {
		if (readCardId(elt->card) == id) {
			card = elt->card;

			return card;
		}

		elt = elt->next;
	}

	return NULL;
}

Card findCardPos(List pack, int pos) {
    Card card;
    Element elt = pack->first;
    int i = 0;
    while (elt != NULL) {
        if (i == pos) {
            card = elt->card;
            
            return card;
        }
        i++;
        elt = elt->next;
    }
    
    return NULL;
}

List switchCardPos(List l, int pos1, int pos2){
	Card first = NULL;
	Card second = NULL;
	int i = 0;
	List temp = createList();
	List temp2 = createList();
	int tmpos = pos1;
	if(pos1 > pos2){
		pos1 = pos2;
		pos2 = tmpos;
	}
	if(pos2 > getListSize(l)){
		return NULL;
	}

	for(i = 0; i <= pos2 && !listIsEmpty(l); i++){
		if(i == pos1)
			first = popFirstCard(l);
		else if(i == pos2)
			second = popFirstCard(l);
		else if(i < pos1)
			appendHeadList(temp,popFirstCard(l));
		else
			appendHeadList(temp2,popFirstCard(l));
	}

	if(second != NULL)
		appendHeadList(l,first);
	while(!listIsEmpty(temp2))
		appendHeadList(l,popFirstCard(temp2));
	if(first != NULL)
		appendHeadList(l,second);
	while(!listIsEmpty(temp))
		appendHeadList(l,popFirstCard(temp));
	freeList(temp);
	freeList(temp2);
	return l;
}

List switchCards(List l, int id1, int id2){
	Card first = NULL;
	Card second = NULL;
	int endflag = 1;
	List temp = createList();
	List temp2 = createList();
	while(!listIsEmpty(l) && endflag){
		first = popFirstCard(l);
		if(first != NULL){
			if(readCardId(first) != id1 && readCardId(first) != id2)
				appendHeadList(temp,first);
			else
				endflag = 0;
		}
	}
	displayList(l);
	endflag = 1;
	while(!listIsEmpty(l) && endflag){
		second = popFirstCard(l);
		if(second != NULL){
			if(readCardId(second) != id1 && readCardId(second) != id2)
				appendHeadList(temp2,second);
			else
				endflag = 0;
		}
	}
	if(second != NULL)
		appendHeadList(l,first);
	while(!listIsEmpty(temp2))
		appendHeadList(l,popFirstCard(temp2));
	if(first != NULL)
		appendHeadList(l,second);
	while(!listIsEmpty(temp))
		appendHeadList(l,popFirstCard(temp));
	freeList(temp);
	freeList(temp2);
	return l;
}

Card popId(List l, int id) {
	if(readCardId(l->first->card) == id)
		return popFirstCard(l);

	Card card;
	Element elt = l->first;
	Element cur = l->first;

	while (elt != NULL) {
		if (readCardId(elt->card) == id) {
			card = elt->card;
			cur->next = elt->next;
			/*free(elt->card);*/
			free(elt);

			return card;
		}
		cur = elt;
		elt = elt->next;
	}

	return NULL;
}

Card popFirstCard(List l)
{
	Element first = l->first;
	if(first == NULL)
		return NULL;
	else if(readCardId(readCardE(first)) == -1)
		return NULL;
	Card c = first->card;
	l->first = first->next;
	free(first);
	return c;
}

Element readFirst(List l) { return l->first; }

Element skipCard(Element e) { return e->next; }

Card readCardE(Element e) { return e->card; }

void displayList(List l)
{
	Element e = l->first;
	while(e != NULL)
	{
		printf("%d->",readCardId(readCardE(e)));
		e = e->next;
	}
	printf("NULL\n");
}


int creerCardFile(char *file, List pack)
{
	int cardcount = 0;
	int cardid = 0;
	int x, y = 0;
	Ground g;
	FILE *fd = fopen(file, "w");
	if(fd == NULL) { return -1; }

	Card card = NULL;
	Element e = readFirst(pack);
	while(e != NULL)
	{
		cardcount++;
		e = skipCard(e);
	}
	fprintf(fd,"%d\n",cardcount);
	e = readFirst(pack);
	while( e != NULL)
	{
		card = readCardE(e);
		fprintf(fd,"%d\n",cardid);
		for(y=0; y<3; y++)
		{
			for(x=0; x<2; x++)
			{
				g = readCardM(card,x,y);
				if(g == V)
					fprintf(fd,"%c ",'V');
				else if(g == U)
					fprintf(fd,"%c ",'U');
				else if(g == L)
					fprintf(fd,"%c ",'L');
				else if(g == F)
					fprintf(fd,"%c ",'F');
				else if(g == R)
					fprintf(fd,"%c ",'R');
				else if(g == P)
					fprintf(fd,"%c ",'P');

			}
			fprintf(fd,"%c",'\n');
		}
		cardid++;
		e = skipCard(e);
	}
	fclose(fd);
	return 0;
}

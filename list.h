/**
* \file list.h
*
* Toutes les fonctions qui servent à gérer une
* liste de type Card
*/
#ifndef LIST_H
#define LIST_H

#include "listlayer.h"

/**
* \brief Crée une liste vide
* \return Renvoie une variable de type List vide
*/
List createList();

/**
* \brief Détruit une liste
* \param l une variable de type List
*/
void freeList(List l);

/**
* \brief Ajoute un élément de type Card en début de liste
* \param l une variable de type List
* \param card une variable de type Card
*/
List appendHeadList(List l, Card card);

List appendHeadTail(List l, Card card);
List appendHeadListE(List l, Element e);
/**
* \brief Retire un élément de type Card dont l'id vaut id
* \param l une variable de type List
* \param id un entier de type int
* \ Renvoie la carte qui a été retirée
*/
Card popId(List l, int id);

/**
* \brief Retire la première carte d'une List
* \param l une variable de type List
* \ Renvoie la carte qui a été retirée
*/
Card popFirstCard(List l);

/**
* \brief Retourne la taille d'une liste
* \param l une variable de type List
* \return Renvoie la taille de la liste l
*/
int getListSize(List l);

/**
* \brief Permet de savoir si la liste est vide
* \param l une variable de type List
* \return Renvoie 1 si la liste est vide, 0 sinon
*/
int listIsEmpty(List l);

/*
 * \brief remplace la position de deux cartes dans une liste
 */
List switchCardPos(List l, int id1, int id2);

/*
 * \brief remplace la position de deux cartes étant donné leurs ids
 */
List switchCards(List l, int id1, int id2);

/**
* \brief Retourne la première carte trouvée qui possède l'id donnée en paramètre
* \param pack une variable de type List
* \param id une variable de type int
* \return Retourne une variable de type Card dont l'id vaut id, sinon renvoie NULL
*/
Card findCard(List pack, int id);


Card findCardPos(List pack, int pos);

/**
* \brief Renvoie la tuile en queue de liste
* \param l une variable de type List
* \return Renvoie la tuile en queue de liste
*/
Element readFirst(List l);

/**
* \brief Renvoie la tuile suivante
* \param e une variable de type Element
* \return Renvoie la tuile suivante
*/
Element skipCard(Element e);

/**
* \brief Renvoie la card lié à un Element
* \param e une variable de type Element
* \return Renvoie la card liée à une variable de type Element
*/
Card readCardE(Element e);

/*
* \brief Affiche une list sous form cardId1->cardId2->NULL
* \param l une List
*/
void displayList(List l);

List flipList(List l);

#endif

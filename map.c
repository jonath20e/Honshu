#include <stdlib.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "list.h"
#include "layer.h"
#include "map.h"


/**
* \internal Dans cette structure, on souhaite que le tableau cases
* contienne uniquement les états de la grille comme par exemple un Lac
* ou une Ville
*/
struct Map_s {
	int* cases;
	List card_init;
	List card_left;
	int size;
};

Map initMap(int n) {
	if(n < 3)
        	return NULL;
	Map map = (Map) malloc(sizeof(struct Map_s));

	map->cases = (int*) calloc(n * n, sizeof(int));
	map->card_init = createList();
	map->card_left = createList();

	map->size = n;


	/*Si le terrain est de taille n paire, on considérera que son centre est en ([n/2], [n/2]) avec
		[m] la partie entière inférieure de m */
	int m = (n%2 != 0) ? (n/2) - 1 : (n/2) - 1 ;

	Card c = initCard();
	insertCard(map,c,m,m);
	setId(c, -1);
	/*
	Problème avec rules.c : la première tuile doit apparaître dans card_left sinon ne marche
	A résoudre plus tard
	*/
	Card dup_c = initCard();

	setId(dup_c, -1);
	setPos(dup_c, m, m);
	changeRotation(dup_c, TOP);

	int i;
	/* Copie de c vers dup_c */
	for (i = 0; i < 6; i++)
		dup_c->cases[i] = c->cases[i];

	appendHeadList(map->card_left,dup_c);
	appendHeadList(map->card_init,c);
	return map;
}

void freeMap(Map map) {
	free(map->cases);
	freeList(map->card_init);
	freeList(map->card_left);

	free(map);
}


int countPoints(Map m, Point_S p)
{
	int i = 0;
	int points = 0;
	int x = 0;
	int p_f = 2, p_v = 1, p_r = 4, p_l = ((p.l)?2:3);
	int y = 0;
	int ressource = 0;
	int foret = 0;
	int village = getMaxVillage(m,p);
	int lac = 0;
	int usine = 0;
	int points_ressource = 0;
	Ground tuile = VIDE;
	for(x = 0; x < m->size; x++)
	{
		for(y = 0; y < m->size; y++)
		{
			tuile = readMap(m,x,y);
			if(tuile == F)
				foret++;
			else if(tuile == L)
				lac++;
			else if(tuile == U)
				usine++;
			else if(tuile == R)
				ressource++;
		}
	}

	if(p.r)
	{
		if(usine > ressource)
			points_ressource = usine - (usine - ressource);
		else if(ressource >=  2*usine)
			points_ressource = ressource - (ressource - 2*usine);
		else if(ressource < 2*usine)
			points_ressource = ressource;
		else
			points_ressource = ressource;
	}
	else
	{
		if(usine > ressource)
			points_ressource = usine - (usine - ressource);
		else if(ressource > usine)
			points_ressource = ressource - (ressource - usine);
		else
			points_ressource = ressource;
	}

	if(p.f)
	{
		p_f = 0;
		for(i=0; i<foret;i++)
		{
			p_f = p_f + (i+1);
		}
		foret = 1;
	}

	points = foret*p_f + lac*p_l + points_ressource*p_r + village*p_v;

	if(p.p)
	{
		points += countPlaines(m);
	}

	return points;
}


int setCountPointsDetail(Map m, int* pforet, int* pvillage, int* pusine, Point_S p)
{
	int points = 0;
	int x = 0;
	int y = 0;
	int ressource = 0;
	int p_f = 2, p_v = 1, p_r = 4, p_l = ((p.l)?2:3);
	int foret = 0;
	int village = getMaxVillage(m,p);
	int lac = 0;
	int usine = 0;
	int points_ressource = 0;
	Ground tuile = VIDE;
	for(x = 0; x < m->size; x++)
	{
		for(y = 0; y < m->size; y++)
		{
			tuile = readMap(m,x,y);
			if(tuile == F)
				foret++;
			else if(tuile == L)
				lac++;
			else if(tuile == U)
				usine++;
			else if(tuile == R)
				ressource++;
		}
	}
	if(usine > ressource)
		points_ressource = usine - (usine - ressource);
	else if(ressource > usine)
		points_ressource = ressource - (ressource - usine);
	else
		points_ressource = ressource;

  (*pusine) = points_ressource * p_r;
  (*pforet) = foret * p_f;
  (*pvillage) = village*p_v;

	points = foret*p_f + lac*p_l + points_ressource*p_r + village*p_v;
	return points;
}

int countPointsAll(List l, int* sforet, int* svillage, int* susine) {
  if (listIsEmpty(l))
    return -1;

  Element e = readFirst(l);
  Card c;

	int p_f = 2, p_v = 1, p_r = 4;
  *sforet = 0;
  *svillage = 0;
  *susine = 0;

  int usine = 0;
  int ressource = 0;

  int i = 0;

  while (e != NULL) {
    c = readCardE(e);

    for (i = 0; i < 6; i++) {
      if (readCard(c, i) == F)
        (*sforet)++;
      else if (readCard(c, i) == V)
        (*svillage)++;
      else if (readCard(c, i) == U)
        (usine)++;
      else if (readCard(c, i) == R)
        (ressource)++;
    }

    e = skipCard(e);
  }
  (*sforet) *= p_f;
  (*svillage) *= p_v;
  (*susine) = p_r * (usine > ressource ? ressource : usine);

  return 1;
}



int countPointsDetail(Map m, Point_S p)
{
	int i = 0;
	int points = 0;
	int x = 0;
	int p_f = 2, p_v = 1, p_r = 4, p_l = ((p.l)?2:3);
	int y = 0;
	int ressource = 0;
	int foret = 0;
	int village = getMaxVillage(m,p);
	int lac = 0;
	int usine = 0;
	int points_ressource = 0;
	int cp;
	Ground tuile = VIDE;
	for(x = 0; x < m->size; x++)
	{
		for(y = 0; y < m->size; y++)
		{
			tuile = readMap(m,x,y);
			if(tuile == F)
				foret++;
			else if(tuile == L)
				lac++;
			else if(tuile == U)
				usine++;
			else if(tuile == R)
				ressource++;
		}
	}

	if(p.r)
	{
		if(usine > ressource)
			points_ressource = usine - (usine - ressource);
		else if(ressource >=  2*usine)
			points_ressource = ressource - (ressource - 2*usine);
		else if(ressource < 2*usine)
			points_ressource = ressource;
		else
			points_ressource = ressource;
	}
	else
	{
		if(usine > ressource)
			points_ressource = usine - (usine - ressource);
		else if(ressource > usine)
			points_ressource = ressource - (ressource - usine);
		else
			points_ressource = ressource;
	}

	if(p.f)
	{
		p_f = 0;
		for(i=0; i<foret;i++)
		{
			p_f = p_f + (i+1);
			printf("%d..",p_f);
		}

		printf("%d forêts: %d points\n",foret,p_f);
	}
	else
	{
		printf("%d forêts: %d points\n",foret,foret*p_f);
	}
	printf("%d lacs: %d points\n",lac,lac*p_l);
	printf("%d usines et %d ressources: %d points\n",usine,ressource,points_ressource*p_r);
	printf("%d villages: %d points\n",village,village*p_v);

	if(p.f)
		foret = 1;
	points = foret*p_f + lac*p_l + points_ressource*p_r + village*p_v;

	if(p.p)
	{
		cp = countPlaines(m);
		printf(" bonus plaines : %d points\n",cp);
		points += cp;
	}

	return points;
}

int readMap(Map map, int x, int y) {
	if (x < 0 || x >= map->size || y < 0 || y >= map->size)
		return -1;

	return map->cases[map->size * x + y];
}


/**
 * \internal l'insertion se fait de bas en haut pour les rotations TOP et BOTTOM
 * \internal l'insertion se fait de gauche vers la droite pour les rotations LEFT et RIGHT
 * \internal le sense d'indexation est inversé pour BOTTOM et partiellement inversé pour LEFT et RIGHT
 */
int insertCard(Map map, Card card, int x, int y)
{
    if (x < 0 || x >= map->size || y < 0 || y >= map->size)
        return -1;
    else if((x+2 > map->size || y+1 > map->size) && (card->rot == TOP || card->rot == BOTTOM))
        return -1;
    else if((x+2 > map->size || y+1 > map->size ) && (card->rot == LEFT || card->rot == RIGHT))
        return -1;
    int i,j = 0;

    for(i=0;i<2;i++)
    {
	for(j=0;j<3;j++)
	{
		if(card->rot==TOP)
			map->cases[map->size * (x+i) + (y+j)] = card->cases[3*i + j];
		else if(card->rot==BOTTOM)
			map->cases[map->size * (x+i) + (y+j)] = card->cases[5 - (3*i + j)];
		if(card->rot==RIGHT)
			map->cases[map->size * (x+j) + (y+i)] = card->cases[3*i + (2-j)];
		else if(card->rot==LEFT)
			map->cases[map->size * (x+j) + (y+i)] = card->cases[(3 - (3*i)) + j];

	}
    }
	card->pos.x = x;
	card->pos.y = y;

	return 0;
}


int refreshGrid(Map m)
{
	memset(m->cases,0,(m->size * m->size * sizeof(int)));
	Card i = readCardE(readFirst(m->card_init));
	insertCard(m,i,i->pos.x,i->pos.y);

	m->card_left = flipList(m->card_left);

	Element s = readFirst(m->card_left);
	if(s == NULL)
		return -1;
	Card d = NULL;
	while(s != NULL) {
		d = readCardE(s);
		insertCard(m,d,d->pos.x,d->pos.y);
		s = skipCard(s);
	}

	m->card_left = flipList(m->card_left);
	return 0;
}


Card retirerCard(Map m)
{
	Card c = popFirstCard(m->card_left);
	if(c == NULL)
		return NULL;
	else
	{
		refreshGrid(m);
		return c;
	}

	return c;
}

Card retirerCardId(Map m, int id)
{
	Card c = popId(m->card_left,id);
	if(c == NULL)
		return NULL;
	else
	{
		refreshGrid(m);
		return c;
	}

	return c;
}


int getSize(Map map) {return map->size;}

/**
 * \internal affichage de la carte en console.
 */
void afficherCarte(Map m)
{
    int x,y;

	fflush(stdout);
	printf(" ");

	for(x=0;x<getSize(m);x++) {
		printf(" %d", x % 10);
	}

	printf("\n");

    for(x=0;x<getSize(m);x++)
    {	printf("%d", x % 10);
        for(y=0;y<getSize(m);y++)
        {
            afficherCase(readMap(m,y,x));
        }
        printf("%s","\n");
    }
}

List getMapList(Map m) { return m->card_left; }

/**
 * \internal les fonctions suivants sont utilisés pour la recherche de village, déplacé au lot c.
 */

struct Point{
    int x;
    int y;
    point *p;
};

int nonVide(point *c)
{
	if(c == NULL)
		return 0;
	else
		return 1;
}

point* initPoint(point *c, int x, int y)
{
    c = malloc(sizeof(point));
    c->x = x;
    c->y = y;
    c->p = NULL;
    return c;
}

point* pushPoint(point *c, int x, int y)
{
    if(c == NULL)
	    return initPoint(c,x,y);
    point *p = malloc(sizeof(point));
    p->x = x;
    p->y = y;
    p->p = c;
    c = p;
    return c;
}

point* popPoint(point *c)
{
    point *p = c->p;
    free(c);
    return p;
}

bool inChain(point *c, int x, int y)
{
	if(c == NULL)
		return false;

	point* t = c;
	while(t->p != NULL)
	{
		if((t->x == x) && (t->y == y))
		{
		    return true;
		}
		t = t->p;
	}
	if(t->x == x && t->y == y)
	{
		return true;
	}

	return false;
}

point* freePoint(point *p)
{
	if(p != NULL)
	{
		point *ref = NULL;
		while(p->p != NULL)
		{
			ref = p->p;
			if(p != NULL)
				free(p);
			p = NULL;
			p = ref;
		}
		if(p != NULL)
			free(p);
		p = NULL;
	}
	return p;
}




/**
 *\brief recherche récursivement un chemin vers un certain tuile pour voire si ils sont connéctées.
 *\param m la carte dans lequel se fait la rechereche
 *\param startx Le coordonnée x de la tuile candidate
 *\param starty Le coordonnée y de la tuile candidate
 *\param x Le coordonnée x de la tuile courante
 *\param y Le coordonnée y de la tuile courante
 *\param p Un pointeur vers un struct point pour stoquer les points déjàs recherchés
 * \return true si connecté false sinon
 */
bool isConnected(Map m, int startx, int starty, int x, int y, point *p, Ground g)
{
    if(startx==x && starty==y)
    {
        return true;
    }
    bool connected = false;
    if(0 <= x-1)
    {
        if((Ground)readMap(m,x-1,y) == g  && !inChain(p,x-1,y))
        {
            p = pushPoint(p,x,y);
            connected = connected || isConnected(m,startx,starty,x-1,y,p,g);
            p = popPoint(p);
        }
    }
    if(x+1 < getSize(m) && !connected)
    {
        if((Ground)readMap(m,x+1,y) == g && !inChain(p,x+1,y))
        {
            p = pushPoint(p,x,y);
            connected = connected || isConnected(m,startx,starty,x+1,y,p,g);
            p = popPoint(p);
        }
    }
    if(0 <= y-1 && !connected)
    {
        if((Ground)readMap(m,x,y-1) == g && !inChain(p,x,y-1))
        {
            p = pushPoint(p,x,y);
            connected = connected || isConnected(m,startx,starty,x,y-1,p,g);
            p = popPoint(p);
        }
    }
    if(y+1 < getSize(m) && !connected)
    {
        if((Ground)readMap(m,x,y+1) == g && !inChain(p,x,y+1))
        {
            p = pushPoint(p,x,y);
            connected = connected || isConnected(m,startx,starty,x,y+1,p,g);
            p = popPoint(p);
        }
    }

    return connected;
}


int countSquareVillage(Map m,int i, int j)
{

	if(readMap(m,i,j) == V)
	{
		if(readMap(m,i+1,j) == V
				&& readMap(m,i,j+1) == V
				&& readMap(m,i+1,j+1) == V){
			return 4;
		}
	}

	return 0;
}


point* copyPoint(point* p)
{
	point* tmv = NULL;;
	point *cp = p;
	while(cp != NULL)
	{
		tmv = pushPoint(tmv,cp->x,cp->y);
		cp = cp->p;
	}
	return tmv;
}




/**
 *\brief retourn la taille du plus grand village
 *\param Un struct map
 */
int getMaxVillage(Map m, Point_S ps)
{
    int x, y, i, j, taille_village, max = 0;
    point* t = NULL;
    point* p = NULL;
    point* mv = NULL;
    point* cp = NULL;
    point* smv = NULL;

    for(x=0;x < getSize(m) ;x++)
    {
        for(y=0;y<getSize(m);y++)
        {
            if(readMap(m,x,y) == (Ground)V && !inChain(t,x,y))
            {
		taille_village = 0;
		t = pushPoint(t,x,y);
		mv = pushPoint(mv,i,j);
		for(i=0; i<getSize(m); i++)
		{
        		for(j=0; j<getSize(m); j++)
        		{
				if(readMap(m,i,j) == V)
		    		{
					p = initPoint(p,i,j);
					if(isConnected(m,x,y,i,j,p,(Ground)V))
					{
						t = pushPoint(t,i,j);
						taille_village++;
						mv = pushPoint(mv,i,j);
					}
					freePoint(p);
		    		}
			}
	    	}
		if(taille_village >= max)
		{
			max = taille_village;
			smv = freePoint(smv);
			smv = copyPoint(mv);
		}
		mv = freePoint(mv);

            }
        }
    }
    freePoint(t);

    if(ps.v)
    {
	if(nonVide(mv))
	{
		cp = smv;
		while(cp != NULL)
		{
			max += countSquareVillage(m,cp->x,cp->y);
			cp = cp->p;
		}
		printf("done.\n");
		fflush(stdout);
	}

    }
    mv = freePoint(mv);
    smv = freePoint(smv);
    return max;
}

int countPlaines(Map m)
{
    int x, y, i, j, score = 0, maxp = 0, taille_village = 0, max = 0;
    point* t = NULL;
    point* p = NULL;
    point* mv = NULL;
    point* cp = NULL;
    point* smv = NULL;
    point* tmv = NULL;
    do{
	    for(x=0;x < getSize(m) ;x++)
	    {
		for(y=0;y<getSize(m);y++)
		{
		    if(readMap(m,x,y) == (Ground)P && !inChain(t,x,y) && !inChain(tmv,x,y))
		    {
			taille_village = 0;
			t = pushPoint(t,x,y);
			mv = pushPoint(mv,i,j);
			for(i=0; i<getSize(m); i++)
			{
				for(j=0; j<getSize(m); j++)
				{
					if(readMap(m,i,j) == P)
					{
						p = initPoint(p,i,j);
						if(isConnected(m,x,y,i,j,p,(Ground)P))
						{
							t = pushPoint(t,i,j);
							taille_village++;
							mv = pushPoint(mv,i,j);
						}
						freePoint(p);
					}
				}
			}
			if(taille_village >= max)
			{
				max = taille_village;
				smv = freePoint(smv);
				smv = copyPoint(mv);
			}

			mv = freePoint(mv);

		    }
		}
	    }
	    t = freePoint(t);

		if(nonVide(smv))
		{
			cp = smv;
			while(cp != NULL)
			{
				tmv = pushPoint(tmv,cp->x,cp->y);
				cp = cp->p;
			}
		}

	    mv = freePoint(mv);
	    smv = freePoint(smv);
	    maxp = max;
	    score += max/4;
	    max = 0;
    }while(maxp > 3);
    tmv = freePoint(tmv);
    return score*4;
}

/**
* \file solveur.h
*
* Toutes les fonctions qui servent à résoudre une
* partie
*/
#ifndef SOLVEUR_H
#define SOLVEUR_H

/*
 * \brief interprétation du solveur fourni en annexe.
 * \param m une Carte
 * \param l une Liste
 * \param depth indicateur du profondeur des appels récursives
 * \return le nombre maximum de cartes pour un arrangement donnée de cartes
 */
int solveur(Map m, List l, int depth,Point_S ps);

/*
 * \brief Solveur qui utilise des comparaisons de score
 * \param m une Carte
 * \param l une Liste
 * \return score obtenu
 */
int solveur_ver1(Map m, List l, Point_S ps);

/*
 * \brief Solveur qui utilise des comparaisons de "distance" de score comme une sorte d'heuristique
 * \param m une Carte
 * \param l une Liste
 * \return score obtenu
 */
int solveur_distance(Map m, List l, Point_S ps);

/*
 * \brief le même solveur que solveur() mais en tentant de ne pas recouvrir des forêts
 */
int solveur_foret(Map m, List l, int depth, Point_S ps);

int solveur_recuit(Map map, List l, Point_S ps);
/*
 * \brief itère tous les combinaisons possibles de cartes en utilisant l'algorithme de Heap
 */
List iterate(Map m, List l, Point_S ps);

#endif

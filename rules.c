#include <stdlib.h>
#include <stdio.h>

#include "layer.h"
#include "list.h"
#include "map.h"
#include "rules.h"


int respecteRegles(Map m, Card c){
	if(limitArea(m,c) == 1) {
		return 0;
	}
	else if(lacTest(m,c) == 1){
		return 0;
	}
	else if(coverTest(m,c) == 0){
		return 0;
	}
	return 1;
}

int coverTest(Map m, Card c) {
	if(m == NULL || c == NULL){
		printf("m or c was null\n");
		return 0;
	}
	Element l = readFirst(getMapList(m)); /* A modifier left*/
	if(l == NULL)
	{
		printf("map list empty!\n");
		return 0;
	}
	Card card;
	
	int compteur = 0;
	
	int i;
	int j;
	
	int k;
	
	int i_r;
	int j_r;
	
	struct Position pos;
	int index = 1;
	
	int tab[6] = {0};
	int tab_test[6] = {0};
	
	int* placement = (int*) calloc(getSize(m) * getSize(m), sizeof(int));
	int sizeCard = getListSize(getMapList(m));
	Card* cardL = (Card*) malloc(sizeof(Card) * sizeCard);
	
	while (l != NULL) {
		card = readCardE(l);
		cardL[compteur] = card;
		
		compteur++;
		l = skipCard(l);
	}
	compteur = 0;
	
	for (k = sizeCard - 1; k >= 0; k--) {
		pos = readPos(cardL[k]);
		
		if (readRot(cardL[k]) == TOP || readRot(cardL[k]) == BOTTOM) {
			i_r = 2 + pos.x;
			j_r	= 3 + pos.y;
		}else {
			i_r = 3 + pos.x;
			j_r	= 2 + pos.y;
		}
		for (i = pos.x; i < i_r; i++)
			for (j = pos.y; j < j_r; j++) 
				placement[getSize(m) * j + i] = index;
				
		index++;
	}
	
	free(cardL);
	/* debug 
	for (i = 0; i < getSize(m); i++) {
		for (j = 0; j < getSize(m); j++) 
			printf(" %d", placement[getSize(m) * i + j]);
		printf("\n");
	}
	*/
	
	int compteur_zero = 0;
	pos = readPos(c);
	
	if (readRot(c) == TOP || readRot(c) == BOTTOM) {
		i_r = 2 + pos.x;
		j_r	= 3 + pos.y;
	}else {
		i_r = 3 + pos.x;
		j_r	= 2 + pos.y;
	}
	for (i = pos.x; i < i_r; i++)
		for (j = pos.y; j < j_r; j++) {
			tab[compteur] = placement[getSize(m) * j + i];
			/* debug  printf("mat %d:%d %d\n",i , j, placement[getSize(m) * j + i]);*/
			placement[getSize(m) * j + i] = index;
			
			if (tab[compteur] == 0)
				compteur_zero++;
			compteur++;
		}
	
	/*printf("Compteur zero : %d\n", compteur_zero);*/
	/*Test si la tuile ne recouvre aucune autre tuile*/
	if (compteur_zero == 6) {
		/*printf("superposition zero\n");*/
		free(placement);
		return 0;
	}

	for (i = 0; i < getSize(m); i++)
		for (j = 0; j < getSize(m); j++) 
			for (k = 0; k < 6; k++)
				if (placement[getSize(m) * i + j] == tab[k])
					tab_test[k] = 1;
	compteur = 0;
	for (k = 0; k < 6; k++)
		if (tab_test[k])
			compteur++;
	if (compteur == 6)
	{
		free(placement);
		return 1;
	}
	else 
	{
		free(placement);
		return 0;
	}
	free(placement);
}


int lacTest(Map map, Card card) {
	struct Position pos = readPos(card);
	int i_r;
	int j_r;
	
	if (readRot(card) == TOP || readRot(card) == BOTTOM) {
		i_r = 2 + pos.x;
		j_r	= 3 + pos.y;
	}else {
		i_r = 3 + pos.x;
		j_r	= 2 + pos.y;
	}
	
	int i;
	int j;
	
	for (i = pos.x; i < i_r; i++) 
		for (j = pos.y; j < j_r; j++)
			if (readMap(map, i, j) == (int) L)
				return 1;
	
	return 0;
}

int limitArea(Map map, Card card) {
	int x = readPos(card).x;
	int y = readPos(card).y;
	
	if (x < 0 || y < 0)
		return 1;
	
	if (readRot(card) == LEFT || readRot(card) == RIGHT) {
		if ((x + 3) > getSize(map) || (y + 2) > getSize(map))
			return 1;
		return 0;
	}else {
		if ((x + 2) > getSize(map) || (y + 3) > getSize(map))
			return 1;
		return 0;
	}
	
}





int groundTest(Map map, Card card, Ground g) {
	struct Position pos = readPos(card);
	int i_r;
	int j_r;
	
	if (readRot(card) == TOP || readRot(card) == BOTTOM) {
		i_r = 2 + pos.x;
		j_r	= 3 + pos.y;
	}else {
		i_r = 3 + pos.x;
		j_r	= 2 + pos.y;
	}
	
	int i;
	int j;
	
	for (i = pos.x; i < i_r; i++) 
		for (j = pos.y; j < j_r; j++)
			if (readMap(map, i, j) == (int) g)
				return 1;
	
	return 0;
}






# Honshu Projet Tuteuré

Ceci est le projet tuteuré du jeu Honshu développé par le groupe LUJU MAPE

## Install

pour compiler le jeu:

```
make honshu
```
pour compiler un test des fonctions du lot A, B et C: 

```
make test
```
pour créer de la documentation:
```
make doxy
```
pour compiler le jeu en mode graphique (en cours de dévelopement):
```
make gui
```

## Usage

Une fois le projet compilé, le projet peux être lancé avec:
```
./a.out
```
Un racourci pour lancer le projet avec valgrind est aussi dans le Makefile:
```
make valgrind
```

## Requirements

Le mode graphique requiert SD2 et SDL_image pour fonctionner:
```
sudo apt-get install libsdl2-dev
```
```
apt install libsdl2-image-dev
```

## Contribute

Si vous souhaitez aider a financer ce projet, veuillez contacter jonathan.boaknin@ensiie.com

## Authors

* Petya Tasheva
* Macil Guiddir
* Jonathan Boaknin
* Luke Austin

## License

© ENSIIE 2018

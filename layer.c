#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <time.h>

#include "layer.h"
#include "list.h"

Card initCard() {
	Card card = (Card) malloc(sizeof(struct Card_s));
	int i = 0;

	card->rot = TOP;
	card->id = 0;
	card->pos = (Position) { .x = 0, .y = 0 };
	for (i = 0; i < 6; i++)
		card->cases[i] = (Ground) (int) ((rand() % 6)+1);

	return card;
}


Card copyCardM(Card c) {
	Card card = (Card) malloc(sizeof(struct Card_s));
	int x, y = 0;

	card->rot = readRot(c);
	card->id = readCardId(c);
	card->pos = (Position) { .x = c->pos.x, .y = c->pos.y };
	for (x = 0; x < 2; x++) {
		for(y = 0; y < 3; y++) {
			card->cases[x * 3 + y] = readCardM(c,x,y);
		}
	}

	return card;
}

void cardCopy(Card card, const Card c_copy) {
	int i = 0;

	card->rot = c_copy->rot;
	card->id = c_copy->id;
	card->pos = (Position) { .x = c_copy->pos.x, .y = c_copy->pos.y };
	for (i = 0; i < 6; i++)
		card->cases[i] = c_copy->cases[i];

}

int sizeofCard() { return sizeof(struct Card_s); }

void freeCard(Card card) { free(card); }

Rotation readRot(Card card) { return card->rot; }/*Mettre a jour le tableau des types de terrain*/

Ground readCard(Card card, int x) { return card->cases[x]; }

struct Position readPos(Card card) { return card->pos; }

int readCardId(Card card) { return card->id; }

Ground readCardM(Card card, int x, int y) {
	if((x * 3 + y) < 0 || (x * 3 + y) > 5)
		return VIDE;
	return card->cases[3 * x + y];
}

void changeRotation(Card card, Rotation rot) { card->rot = rot; }


/**
 * \internal initialisation d'une List (ou main) de cartes à partir d'un fichier
 */
List initCardFile(char* file, List pack) {
	int status = 0;
	int cardcount = 0;
	int i = 0;
	int cardid = 0;
	char cardtype = 0;
	int x, y = 0;
	FILE *fd = fopen(file, "r");
	if(fd == NULL) { return NULL; }
	status = fscanf(fd,"%d",&cardcount);
	if(status == -1) { return NULL;}
	Card card = NULL;
	pack = createList();
	for(i=0; i<cardcount && !feof(fd); i++)
	{
		status = fscanf(fd," %d",&cardid);
		if(status == -1) {
			printf("bad status!");
			return NULL;
		}
		card = initCard();
		setId(card,cardid);
		for(y=0; y<3; y++)
		{
			for(x=0; x<2; x++)
			{
				status = fscanf(fd," %c",&cardtype);
				if(status == -1)
				{
					printf("stat error");
					freeList(pack);
					freeCard(card);
					fclose(fd);
					return NULL;
				}
				setGround(card, cardtype, x, y);
			}
		}
		appendHeadList(pack,card);
	}
	fclose(fd);
	return pack;
}

List initCardPack(int taille)
{
	int i = 0;
	Card card = NULL;
	List pack = createList();
	for(i=0; i<taille; i++)
	{
		card = initCard();
		setId(card,i);
		appendHeadList(pack,card);
	}
	return pack;
}

void displayPack(List pack)
{
	Element e = NULL;
	Card c = NULL;
	int row = 0;
	for(row = 0; row < 3; row++)
	{
		e = readFirst(pack);
		while( e != NULL)
		{
			c = readCardE(e);
			if(row == 0)
				printf(" %d ",readCardId(c));
			else
				printf("   ");
			afficherCase(readCardM(c,0,row));
			afficherCase(readCardM(c,1,row));
			printf("  ");
			e = skipCard(e);

		}
		printf("\n");
	}
}

void displayCard(Card card)
{
	int x,y = 0;
	printf("Carte numéro %d\n", readCardId(card));
	if(readRot(card) == TOP || readRot(card) == BOTTOM)
	{
		for(y=0; y<3; y++)
		{
			for(x=0; x<2; x++)
			{
				if(readRot(card) == TOP)
					afficherCase(readCardM(card,x,y));
				else
					afficherCase(readCard(card,5 - (3*x+y)));
			}
			printf("%s","\n");
		}
	}
	else
	{
		for(x=0; x<2; x++)
		{
			for(y=0; y<3; y++)
			{
				if(readRot(card) == RIGHT)
					afficherCase(readCard(card,3*x + (2-y)));
				else
					afficherCase(readCard(card,(3 - (3*x) + y)));
			}
			printf("%s","\n");
		}
	}
	printf("\n");
}

void afficherCase(int t)
{
	if(t == V)
		printf(GRN " %s" RESET,"V");
	else if(t == U)
		printf(MAG " %s" RESET,"U");
	else if(t == L)
		printf(BLU " %s" RESET,"L");
	else if(t == F)
		printf(GRN " %s" RESET,"F");
	else if(t == R)
		printf(CYN " %s" RESET,"R");
	else if(t == P)
		printf(YEL " %s" RESET,"P");
	else
		printf(" %s","_");
}

int setGround(Card card, char cardtype, int  x, int y)
{
	if(cardtype == 'V')
		card->cases[3 * x + y] = V;
	else if(cardtype == 'U')
		card->cases[3 * x + y] = U;
	else if(cardtype == 'L')
		card->cases[3 * x + y] = L;
	else if(cardtype == 'F')
		card->cases[3 * x + y] = F;
	else if(cardtype == 'R')
		card->cases[3 * x + y] = R;
	else if(cardtype == 'P')
		card->cases[3 * x + y] = P;

	return 0;
}

void setId(Card card, int cardid) { card->id = cardid; }
void setPos(Card card, int x, int y) {
	card->pos.x = x;
	card->pos.y = y;
}

void freePack(Card *c,int cardcount)
{
	int i = 0;
	for(i=0; i<cardcount; i++)
	{
		freeCard(c[i]);
	}
	free(c);
}

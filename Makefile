SDL_CFLAGS := $(shell sdl2-config --cflags)
SDL_LDFLAGS := $(shell sdl2-config --libs)
SDL := -I/Library/Frameworks/SDL.framework/Headers -framework SDL2 -framework Cocoa

CC = gcc -Wall -Wextra -ansi

goal: honshu

test: list.o layer.o map.o test.o rules.o
	$(CC) -g layer.o list.o map.o test.o rules.o -lcunit

gui: list.o layer.o map.o fenetre.o rules.o solveur.o
	$(CC) -g layer.o list.o map.o fenetre.o rules.o solveur.o -lcunit -lSDL2_image -lm $(SDL_LDFLAGS)

test.o: test.c
	$(CC) -c test.c

fenetre.o: fenetre.c
	$(CC) -c fenetre.c $(SDL_CFLAGS)

honshu: list.o layer.o map.o honshu.o rules.o solveur.o
	$(CC) -g layer.o list.o map.o honshu.o rules.o solveur.o -lm

honshu.o: honshu.c
	$(CC) -c honshu.c

list.o: list.c listlayer.h
	$(CC) -c list.c

layer.o: layer.c listlayer.h
	$(CC) -c layer.c

map.o: map.c map.h
	$(CC) -c map.c

rules.o: rules.c listlayer.h rules.h map.h layer.h
	$(CC) -c rules.c

solveur.o: solveur.c listlayer.h rules.h map.h layer.h
	$(CC) -c solveur.c

doxy:
	doxygen config-file

valgrind: a.out
	valgrind --leak-check=yes ./a.out

clean:
	rm *.o

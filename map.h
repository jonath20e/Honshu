/**
* \file map.h
*
* Toutes les fonctions qui servent à gérer la
* grille du jeu
* En incluant map.h, layer.h sera également inclus à votre projet
*/
#ifndef MAP_H
#define MAP_H


/**
* \struct Map
* \brief Objet Map
*
* Map est un objet qui permet de contenir une grille
* et de gérer les tuiles posées par le joueur par
* les fonctions de l'interface map.h
*/
typedef struct Map_s* Map;

typedef struct Point_Struct{
	int l;
	int p;
	int r;
	int v;
	int f;
}Point_S;

/**
* \brief Crée une grille de taille n * n
* \param n un entier positif supérieur ou égale à 3
* \attention n doit être supérieur ou égale, sinon
* des erreus peuvent survenir
* \return Une grille de taille n * n de type Map
* \attention si n est inférieur à 3 la fonction renvoie null
* \internal On réalise une allocation dynamique pour
* créer un tableau unidimensionnel de taille n * n
*/
Map initMap(int n);


/**
* \brief Détruit la grille crée par la fonction initMap
* \param map une variable de type Map
* \internal Libère la variable map allouée par initMap
*/
void freeMap(Map map);

/**
 * \brief compte les points à la fin d'une partie
 * \param m une variable de type Map
 * \return le nombre de points gagné
 */
int countPoints(Map m, Point_S p);

/**
 * \brief compte les points à la fin d'une partie
 * \param m une variable de type Map
 * \return le nombre de points gagné
 */
int countPointsDetail(Map m, Point_S p);

/**
 * \brief compte les points
 * \param m une variable de type Map
 * \param pforet un pointeur de type int
 * \param pvillage un pointeur de type int
 * \param pusine un pointeur de type int
 * \return le nombre de points gagné
 * \detail pforet contient le nombre de points forets (idem pour les deux autres variables)
 */
int setCountPointsDetail(Map m, int* pforet, int* pvillage, int* pusine, Point_S p);

/**
 * \brief compte les points potentielles d'une liste
 * \param m une variable de type Map
 * \param pforet un pointeur de type int
 * \param pvillage un pointeur de type int
 * \param pusine un pointeur de type int
 * \return -1 si ça s'est mal passé, 1 sinon
 * \detail pforet contient le nombre de points forets (idem pour les deux autres variables)
 */
int countPointsAll(List l, int* sforet, int* svillage, int* susine);

/**
* \brief Détermine la valeur d'une case dans une grille
* \param map une variable de type Map
* \param x un entier positif et inférieur à la taille de la grille
* \param y un entier positif et inférieur à la taille de la grille
* \return Renvoie la valeur de la case de type Ground à la position (x, y) d'une grille
* sinon -1 si un problème survient
* \internal Simple lecture d'un tableau unidimenionnel du membre cases de la variable map
*/
int readMap(Map map, int x, int y);

/**
 * \brief remet tous les tuiles sur la carte.
 */
int refreshGrid(Map m);

/**
* \brief Supprime une tuile
*/
/*int popCard(Map map, int id);*/


/**
* \brief Renvoie la taille de la Map
*/
int getSize(Map map);

/**
 * \brief un struct pour stocker les coordonnées x / y d'un point
 */
typedef struct Point point;

/**
 *\brief retourne la taille du plus grand village
 */
int getMaxVillage(Map m, Point_S ps);

int countPlaines(Map m);

/**
 *\brief Affiche la carte en console
 *\param Un struct map
 */
void afficherCarte(Map m);

/**
 * \brief insère une carte
 * \param map la carte (map)
 * \param card la carte (tuile)
 * \param x le coordonné x de l'origine de la carte
 * \param y le coordonné y de l'origine de la carte
 * \return -1 en cas d'erreur, 0 sinon
 */
int insertCard(Map map, Card card, int x, int y);

/**
 * \brief retire une carte
 * \param map la grille de jeu
 * \return NULL en cas d'erreur, 0 sinon
 */
Card retirerCard(Map m);

/**
 * \brief retire une carte
 * \param map la grille de jeu
 * \param l'id de la carte a retirer
 * \return NULL en cas d'erreur, 0 sinon
 */
Card retirerCardId(Map m,int id);

/**
 * \brief retourne la Liste init_left d'un Map
 * \param m une Map
 * \return la Liste m->init_left
 */
List getMapList(Map m);

#endif

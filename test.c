#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>

#include "list.h"
#include "layer.h"
#include "map.h"
#include "rules.h"

void test_MenuDuJeu(void)
{
	srand(time(NULL));
	int r = rand()%10+10;
	
	Map m = initMap(r);	

	List p=initCardPack(r);
	CU_ASSERT(p != NULL);

	/*retirerCard*/
	Card c = retirerCard(m);
	CU_ASSERT(c == NULL);

	/*findCard*/
	Card fc = findCard(p,r);
	CU_ASSERT(fc==NULL);

	/*listIsEmpty*/
	int e = listIsEmpty(p);
	CU_ASSERT(e!=-1);

	freeList(p);
	freeMap(m);
}

void test_Recouvrement(void)
{
	Card c = initCard();
	Map m = initMap(16);

	/* Au position 0, 0, coverTest devrait échouer. */
	setPos(c,0,0);
	CU_ASSERT(coverTest(m,c) == 0); /* erreur:0*/
	CU_ASSERT(lacTest(m,c) == 0); /* ok:0 */
	CU_ASSERT(limitArea(m,c) == 0); /* ok: 0 */

	/* Au position max, max, limitArea et coverTest devraient échouer. */
	setPos(c,getSize(m),getSize(m));
	CU_ASSERT(coverTest(m,c) == 0); /* erreur:0 */
	CU_ASSERT(lacTest(m,c) == 0); /* ok:0 */
	CU_ASSERT(limitArea(m,c) == 1); /* erreur:1 */

	/* Au milieu de la grille, poser un tuile devrait être possible. */
	setPos(c,getSize(m)/2,getSize(m)/2);
	CU_ASSERT(coverTest(m,c) == 1); /* ok: 1 */
	CU_ASSERT(limitArea(m,c) == 0); /* ok: 0 */

	/* Directement sur la carte initiale, coverTest devrait échouer. */
	setPos(c,getSize(m)/2-1,getSize(m)/2-1);
	CU_ASSERT(coverTest(m,c) == 0); /* erreur: 0 */ 

	/* En changeant la rotation, poser la tuile devrait être possible. */
	changeRotation(c,RIGHT);
	setPos(c,getSize(m)/2-1,getSize(m)/2-1);
	CU_ASSERT(coverTest(m,c) == 1); /* ok: 1 */ 


	freeCard(c);
	freeMap(m);
}

void test_Map(void)
{
	srand(time(NULL));
	int r = rand()%10+10;
	
	/*Allocation dynamique d’une grille de jeu carrée de taille variable ;*/
	Map m = initMap(r);
	CU_ASSERT(m != NULL);
	CU_ASSERT(initMap(r * -1) == NULL);
	freeMap(m);
}


void test_Card(void)
{
	/*Création d’une tuile connaissant les 6 terrains*/
	Card b = initCard();
	Map m = initMap(16);
	List l = NULL;
	CU_ASSERT(b!=NULL);
	 
	/*Initialisation de la grille et des tuiles de manière aléatoire ; */
	Map randommap = initMap(rand()%10+10);
	CU_ASSERT(randommap != NULL);
	Card randomcard = initCard();
	CU_ASSERT(randomcard != NULL);
	freeMap(randommap);
	freeCard(randomcard);    


	/*Initialisation de la grille et des tuiles à partir d’un fichier (vous devez être capable  de générer lesdits fichiers)*/
	CU_ASSERT(initCardFile("abs.txt",l)==NULL);
	l = initCardFile("tuiles.txt",l);
	CU_ASSERT(l != NULL);
	CU_ASSERT(creerCardFile("tuilestest.txt",l) != -1);

	/*Rotation d’une tuile*/	
	changeRotation(b,LEFT);
	CU_ASSERT(readRot(b) == LEFT);

	/*Insertion d'une tuile connaissant son orientation et la coordonnée de sa case la plus en haut à gauche*/
	insertCard(m,b,3,5);
	
	/*Lecture du terrain sur la case (x, y)*/
	CU_ASSERT(readMap(m,3,5) == (int) readCardM(b,0,0));
	CU_ASSERT(insertCard(m,b,-1,300)==-1);
	CU_ASSERT(insertCard(m,b,getSize(m)+3,0) == -1);


	/*Libération de l’espace mémoire occupé par une grille*/
	freeCard(b);
	freeMap(m);
	freeList(l);
}

int main (void) {

	CU_pSuite pSuite = NULL;
	CU_initialize_registry();

	if (CU_initialize_registry() != CUE_SUCCESS) {
		return CU_get_error();
	}

	pSuite = CU_add_suite("Suite",NULL,NULL);

	if (pSuite==NULL) {
		CU_cleanup_registry();
		return CU_get_error();
	}

    	if ((CU_add_test (pSuite, "test_Card()",test_Card)==NULL) 
       	|| (CU_add_test(pSuite, "test_Map()",test_Map)==NULL)
       	|| (CU_add_test(pSuite, "test_Recouvrement()",test_Recouvrement)==NULL)
       	|| (CU_add_test(pSuite, "test_MenuDuJeu()",test_MenuDuJeu)==NULL)){
        	CU_cleanup_registry();
        	return CU_get_error();
    	}
	CU_basic_set_mode(CU_BRM_VERBOSE);
	CU_basic_run_tests();
	CU_cleanup_registry();
	return EXIT_SUCCESS;
}


#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <string.h>

#include "list.h"
#include "layer.h"
#include "map.h"
#include "rules.h"
#include "solveur.h"


void afficherRegles(Point_S ps){
	printf("Régles:\t Lac: %d points  Forêt: %s points Usine+Ressource%s: 4 points\n",(ps.l?2:3),(ps.f?"1 à 5":"2"),(ps.r?"[+Ressource]":""));
	printf("\t Plaine: 0 points Ville: 1 point.  \n");
	printf("\t Seule le plus grand rassemblement de villes contiguës est compté.\n");
	printf("\t Une seule usine par ressource.\n");
	printf("\t Un lac ne peut pas être recouvert.\n");
	
	printf("\tVariante en vigeur:\n");
	if(ps.p)
		printf("\tUne plaine de 4 cases vaut 4 points\n");
	else if(ps.l)
		printf("\tUn lac vaut 2 points\n");
	else if(ps.v)
		printf("\tUn carré de 4 villages donne 4 points bonus\n");
	else if(ps.f)
		printf("\tLa première forêt vaut 1, puis 2, jusqu'a 5\n");
	else if(ps.r)
		printf("\tUne usine peux accepter deux ressources.\n");
	else
		printf("\tAucune.\n");
	fflush(stdout);
}

void afficherReglesM(){
    printf("1. Regardez le plus grand village (plus grand bloc de cases Ville (V) contigües). Chacune des cases V vous rapporte 1 point ; \n");
    printf("2. Comptez les Forêts. Chacune des cases Forêt (F) vous rapporte 2 points ; \n");
    printf("3. Regardez les Lacs (L). Il y a l cases de type ''Lacs''. Vous gagnez 3x(l-1) points ; \n");
    printf("4. Comptez les Ressources (R). On alloue une Ressource par Usine (U) tant qu’il reste des cases R et des cases U. Chaque Ressource allouée à une Usine rapporte 4 points. Attention, chaque Usine ne peut traiter qu’une Ressource et une Ressource ne peut être allouée qu’à une Usine. Enfin, une Usine peut traiter une ressource d’une case qui ne lui est pas contigüe.\n");
}


void menuPoserCarte(Card c, Map m, List pack)
{
	char err[50] = {""};
	char option = '0';
	int x = 0;
	int y = 0;
	do
	{
		system("clear");
		afficherCarte(m);
		/*displayPack(pack);*/
		printf("Tuiles : ");
		displayList(pack);
		printf("Grille : ");
		displayList(getMapList(m));
		displayCard(c);
		printf("%s\n", err);
		printf("Que voulez-vous faire?\n");
		printf("d-g-h-b) Tourner p) Poser  r) Retour: ");
		scanf(" %c",&option);
		if(option == 'd')
			changeRotation(c,RIGHT);
		else if(option == 'g')
			changeRotation(c,LEFT);
		else if(option == 'h')
			changeRotation(c,TOP);
		else if(option == 'b')
			changeRotation(c,BOTTOM);
		else if(option == 'p')
		{
			printf("Coordonnée x : ");
			scanf(" %d",&x);
			printf("Coordonnée y : ");
			scanf(" %d",&y);
			setPos(c, x, y);
			if(limitArea(m, c)) {
				strcpy(err,"Coordonnées invalides !\n");
			}else if(lacTest(m, c)) {
				strcpy(err, "Lac touché !\n");
			}else if(!coverTest(m, c)) {
				strcpy(err, "Superposition non autorisée !\n");
			}else if(insertCard(m,c,x,y) == -1)
				strcpy(err,"Coordonnées invalides !\n");
			else
			{
				appendHeadList(getMapList(m),c);
				popId(pack,readCardId(c));
				printf("Tuiles disponibles ---> Grille");
				printf("Tuiles disponibles : ");
				displayList(pack);
				printf("Grille : ");
				displayList(getMapList(m));
				option = 'r';
			}
		}
	}while(option != 'r');
	system("clear");
}

void runGame()
{
	srand(time(NULL));
	char option;
	int n;
	clock_t start, end;
	Card c = NULL;
	List pack = NULL;
	Map m = NULL;
	printf("Choix du chargement de la grille de jeu\n");
	printf("f) Charger un fichier\nr) Grille aléatoire\n");
	scanf(" %c",&option);

	char file[255];

	if (option == 'f') {
		printf("Nom du fichier ?\n");
		scanf(" %s", file);
		pack = initCardFile(file,pack);
		m = initMap(getListSize(pack) + 4);
	}
	else
	{
		m = initMap(rand()%3+5); /* taille de la grille entre 5 et 8 */
		pack = initCardPack(getSize(m) - 3);
		/*iterate(m,pack);*/

	}
	Point_S ps = { .l = 0, .p = 0, .r = 0, .v = 0, .f = 0};
	Point_S backup = { .l = 0, .p = 0, .r = 0, .v = 0, .f = 0};
	int variant = rand()%6;
	if(variant == 0)
		ps.l = 1;
	else if(variant == 1)
		ps.p = 1;
	else if(variant == 2)
		ps.r = 1;
	else if(variant == 3)
		ps.v = 1;
	else if(variant == 4)
		ps.f = 1;

	pack = flipList(pack);
	pack = flipList(pack); 

	afficherRegles(ps);

	printf("Points : %d\n",countPoints(m,ps));
	do
	{
		afficherCarte(m);
		displayPack(pack);
		printf("Score actuel: %d\n",countPoints(m,ps));
		printf("Que voulez-vous faire?\n");
        	printf("q) Quitter\n 1-%d) Sélectionner une tuile \n r) Retirer une tuile s) Utiliser le solveur \n",getListSize(pack));
        	printf("Solveurs optimisés: f) Maximiser les forêts d) distance o) Utiliser le solveur optimisé e) Rappeler les règles: ");
		scanf(" %c",&option);
		system("clear");
		if(option == 'e')
		{
			afficherRegles(ps);
		}
		if(option == 'r')
		{
			c = retirerCard(m); /* A tester */
			if(c == NULL)
				printf("Impossible d'enlever une carte : liste vide\n");
			else
			{
				pack = appendHeadList(pack,c);
				printf("Grille ---> Tuiles \n");
				printf("Tuiles : ");
				displayList(pack);
				printf("Grille : ");
				displayList(getMapList(m));
				option = 'r';
			}
		}
		else if(option == 's')
		{
			start = clock();
			/*solveur(m,pack,0);*/
			pack = iterate(m,pack,ps);
			end = clock();
			printf("Le solveur a pris %lf secondes\n",((double)end-start)/CLOCKS_PER_SEC);
		}
		else if(option == 'f')
		{
			start = clock();
			solveur_foret(m,pack,0,ps);
			/*iterate(m,pack);*/
			end = clock();
			printf("Le solveur optimisé a pris %lf secondes\n",((double)end-start)/CLOCKS_PER_SEC);
		}
		else if(option == 'd')
		{
		    start = clock();
		    solveur_distance(m,pack,backup);
		    end = clock();
		    printf("Le solveur optimisé a pris %lf secondes\n",((double)end-start)/CLOCKS_PER_SEC);
		}
		else if(option == 'o')
		{
		    start = clock();
		    solveur_ver1(m,pack,backup);
		    end = clock();
		    printf("Le solveur optimisé a pris %lf secondes\n",((double)end-start)/CLOCKS_PER_SEC);
		}
		else if(option == 'c')
		{
		    start = clock();
		    solveur_recuit(m,pack,backup);
		    end = clock();
		    printf("Le solveur optimisé a pris %lf secondes\n",((double)end-start)/CLOCKS_PER_SEC);
		}
		else if(option != 'q')
		{
			n = (int)option - '0';
			c = findCard(pack,n); /* A tester */
			if(c != NULL)
				menuPoserCarte(c,m,pack);
		}
	}while(option != 'q');
	printf("---Fin de partie---\n");
	afficherCarte(m);
	int points = countPointsDetail(m,ps);
	printf("Vous avez gagné %d points !\n",points);
	option = 'q';
	freeMap(m);
	freeList(pack);
}


int main(void)
{

    srand(time(NULL));

	/* Affichage d'une banière */
    	int i;
	printf("\t\t");
	for (i = 0; i < 50; i++)
		printf("*");
	printf("\n");

	i = 0;

	printf("\t\t");
	while (i < 50) {
		if (i == 22) {
			printf("Honshu");
			i += 5;
		}else
			printf("*");
		i++;
	}
	printf("\n\t\t");

	for (i = 0; i < 50; i++)
		printf("*");
	printf("\n\n");
	runGame();
    fflush(stdout);
   return EXIT_SUCCESS;
}

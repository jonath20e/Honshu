#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <math.h>

#include "layer.h"
#include "list.h"
#include "map.h"
#include "rules.h"
#include "solveur.h"

int max(int a, int b) {return (a > b) ? a : b;}

int solveur_ver1(Map m, List l, Point_S ps) {
    int best = 0;
    if (listIsEmpty(l))
        return countPoints(m,ps);

    int foret;
    int village;
    int usine;

    countPointsAll(l, &foret, &village, &usine);
    printf("Forets = %d\n Village = %d\n Ressource-Usine = %d\n", foret, village, usine);

    Ground best_g;

    if (foret > village && village > usine)
        best_g = F;
    else if (village > foret && foret > usine)
        best_g = V;
    else
        best_g = U;

    Element e = readFirst(l);

    Card c = initCard();
    Card c_current = readCardE(e);

    int x = 0;
    int y = 0;
    int r = 0;

    int best_tmp = 0;

    int compteur = 0;


    while (!listIsEmpty(l)) {
        countPointsAll(l, &foret, &village, &usine);
        if (foret > village && village > usine)
            best_g = F;
        else if (village > foret && foret > usine)
            best_g = V;
        else
            best_g = U;

        while (e != NULL) {
            setId(c, -1);
            c_current = readCardE(e);

            for (x = 0; x < getSize(m); x++) {
                for (y = 0; y < getSize(m); y++) {
                    for (r = 0; r < 4; r++) {
                        changeRotation(c_current, (Rotation) r);
                        setPos(c_current,x,y);
                        if(respecteRegles(m, c_current)) {
                            insertCard(m,c_current,x,y);
                            appendHeadList(getMapList(m),c_current);

                            if (readCardId(c) == -1)
                                cardCopy(c, c_current);

                            setCountPointsDetail(m, &foret, &village, &usine,ps);
                            if (best_g == F && foret > best_tmp) {
                                best_tmp = foret;
                                cardCopy(c, c_current);
                            }else if (best_g == U && usine > best_tmp) {
                                best_tmp = usine;
                                cardCopy(c, c_current);

                            }else if(best_g == V && village > best_tmp){
                                best_tmp = village;
                                cardCopy(c, c_current);

                            }
                            retirerCard(m);
                        }
                    }
                }
            }

            e = skipCard(e);


        }
        best_tmp = 0;
        compteur++;

        printf("(x, y) = (%d, %d)\n", readPos(c).x, readPos(c).y);
        printf("compteur, id %d, %d\n", compteur, readCardId(c));
        popId(l, readCardId(c));
        cardCopy(c_current, c);
        insertCard(m,c_current,readPos(c_current).x,readPos(c_current).y);
        appendHeadList(getMapList(m),c_current);

        displayList(getMapList(m));
        displayList(l);
        afficherCarte(m);

        e = readFirst(l);


    }
    best = countPointsDetail(m,ps);
    freeCard(c);
    return best;
}

int solveur_distance(Map m, List l, Point_S ps) {
  int best = 0;
  if (listIsEmpty(l))
    return countPoints(m,ps);

  int foret;
  int village;
  int usine;

  countPointsAll(l, &foret, &village, &usine);
  printf("Forets = %d\n Village = %d\n Ressource-Usine = %d\n", foret, village, usine);

  Element e = readFirst(l);

  Card c = initCard();
  Card c_current = readCardE(e);

  int x = 0;
  int y = 0;
  int r = 0;

  int best_tmp = 0;

  int compteur = 0;

	int village_tmp = 0;
	int foret_tmp = 0;
	int usine_tmp = 0;

  while (!listIsEmpty(l)) {
    printf("Debug\n");
    afficherCarte(m);
    countPointsAll(l, &foret_tmp, &village_tmp, &usine_tmp);
    while (e != NULL) {
      setId(c, -2);
      c_current = readCardE(e);

      for (x = 0; x < getSize(m); x++) {
        for (y = 0; y < getSize(m); y++) {
          for (r = 0; r < 4; r++) {
            changeRotation(c_current, (Rotation) r);
            setPos(c_current,x,y);
            if(respecteRegles(m, c_current)) {
              insertCard(m,c_current,x,y);
              appendHeadList(getMapList(m),c_current);

              if (readCardId(c) == -2)
                cardCopy(c, c_current);

              setCountPointsDetail(m, &foret, &village, &usine, ps);
              if (best_tmp < (abs(foret-foret_tmp) + abs(village-village_tmp) + abs(usine-usine_tmp))) {
                  best_tmp = (abs(foret-foret_tmp) + abs(village-village_tmp) + abs(usine-usine_tmp));
                  cardCopy(c, c_current);
              }
              retirerCard(m);
            }
          }
        }
      }

      e = skipCard(e);

    }
    afficherCarte(m);
    best_tmp = 0;
    foret_tmp = 0;
    village_tmp = 0;
    usine_tmp = 0;
    compteur++;

    printf("(x, y) = (%d, %d)\n", readPos(c).x, readPos(c).y);
    printf("compteur, id %d, %d\n", compteur, readCardId(c));
    retirerCardId(m, readCardId(c));
    popId(l, readCardId(c));
    cardCopy(c_current, c);

    printf("\n %d \n", respecteRegles(m ,c_current));
    insertCard(m,c_current,readPos(c_current).x,readPos(c_current).y);
    appendHeadList(getMapList(m),c_current);

    displayList(getMapList(m));
    displayList(l);

    e = readFirst(l);
  }
    freeCard(c);
    best = countPointsDetail(m,ps);
    return best;
}

int solveur(Map m, List l, int depth, Point_S ps) {
	int best = -1;
	if(listIsEmpty(l)){
		best = countPoints(m,ps);
		return best;
	}
	int new_best = -1;
	int best_x = 0;
	int best_y = 0;
	int best_r = 0;
	int return_best = 0;
	int x = 0;
	int y = 0;
	int r = 0;
	Card c = popFirstCard(l);
	while(c != NULL) {
		for(x = 0; x < getSize(m); x++)
		{
			for(y = 0; y < getSize(m); y++)
			{
				for(r = 0; r < 4; r++)
				{
					changeRotation(c, (Rotation) r);
					setPos(c,x,y);
					if(respecteRegles(m, c)) {
						insertCard(m,c,x,y);
						appendHeadList(getMapList(m),c);
						new_best =  solveur(m, l, depth+1,ps);
						if(new_best > best) {
							best_x = x;
							best_y = y;
							best_r = r;
							best = new_best;

							if(depth == 0)
								printf("nouveau max: %d x:%d y:%d r:%d for c:%d...\n",best,best_x,best_y,best_r,readCardId(c));
						}
						retirerCard(m);
					}
				}
			}
		}
		if(depth == 0){
			changeRotation(c,(Rotation) best_r);
			if(insertCard(m,c,best_x,best_y) == -1)
				printf("insert problem!!\n");
			appendHeadList(getMapList(m),c);
			printf("\nCarte placée: id:%d, x:%d, y:%d, r:%d best:%d\n",readCardId(c),best_x,best_y,best_r,best);
			/*displayList(l);
			afficherCarte(m);*/
			return_best = best;
			best = -1;
			c = popFirstCard(l);
		}
		else{
			appendHeadList(l,c);
			return best;
		}
	}
	if(depth != 0)
		printf("here for some reason\n");
	return return_best;
}

void emptyList(List l,Map m){
	Card c = NULL;
	while(!listIsEmpty(l)){
		c = popFirstCard(l);
		insertCard(m,c,c->pos.x,c->pos.y);
		appendHeadList(getMapList(m),c);
	}
}

void emptyMap(Map m,List l){
	Card c = retirerCard(m);
	while(c != NULL){
		appendHeadList(l,c);
		c = retirerCard(m);
	}

	l = flipList(l);
}



List copyList(List b, List l){
	b = createList();
	Element e = readFirst(l);
	Card c = NULL;
	while(e != NULL){
		c = readCardE(e);
		appendHeadList(b, copyCardM(c));
		e = skipCard(e);
	}
	return b;
}

List iterate(Map m, List l, Point_S ps)
{
	int i = 0;
	int n = getListSize(l);
	int* c = NULL;
	int max = -1;

	c = (int*)malloc(n*sizeof(int));
	List best_list = NULL;
	best_list = copyList(best_list,l);
	memset(c,0,n*sizeof(int));

	displayList(l);
	int new_max = solveur(m,l,0,ps);
	printf("solveur returned...%d\n",new_max);
	afficherCarte(m);
	emptyMap(m,l);
	
	printf("new max: %d\n",new_max);
	max = new_max;
	freeList(best_list);
	best_list = copyList(best_list,l);
	
	while(i < n){
		if(c[i] < i){
			if(i%2 == 0){
				switchCardPos(l,0,i);
			}
			else{
				switchCardPos(l,c[i],i);
			}
			displayList(l);
			new_max = solveur(m,l,0,ps);
			emptyMap(m,l);
			if(new_max > max) {
				printf("new max: %d\n",new_max);
				max = new_max;
				freeList(best_list);
				best_list = copyList(best_list,l);
			}
			c[i]++;
			i = 0;
		}
		else{
			c[i] = 0;
			i++;
		}

	}
	free(c);
	emptyList(best_list,m);
	freeList(l);
	l = best_list;
	return l;
}

int solveur_foret(Map m, List l, int depth, Point_S ps) {
	int best = -1;
	if(listIsEmpty(l)){
		best = countPoints(m,ps);
		return best;
	}
	int new_best = -1;
	int best_x = 0;
	int best_y = 0;
	int best_r = 0;
	int x = 0;
	int y = 0;
	int r = 0;
	int fflag = 0;
	Card c = popFirstCard(l);
	while(c != NULL) {
		for(x = 0; x < getSize(m); x++)
		{
			for(y = 0; y < getSize(m); y++)
			{
				for(r = 0; r < 4; r++)
				{
					changeRotation(c, (Rotation) r);
					setPos(c,x,y);
					if(respecteRegles(m, c)) {
						if((groundTest(m,c,F) != 1) || fflag){
							insertCard(m,c,x,y);
							appendHeadList(getMapList(m),c);
							new_best =  solveur(m, l, depth+1,ps);
							if(new_best > best) {
								best_x = x;
								best_y = y;
								best_r = r;
								best = new_best;

								if(depth == 0)
									printf("nouveau max: %d x:%d y:%d r:%d for c:%d...\n",best,best_x,best_y,best_r,readCardId(c));
							}
							retirerCard(m);
						}
					}
				}
			}
		}
		if(best == -1){
			fflag = 1;
		}
		else{
			if(depth == 0){
				changeRotation(c,(Rotation) best_r);
				if(insertCard(m,c,best_x,best_y) == -1)
					printf("insert problem!!\n");
				appendHeadList(getMapList(m),c);
				printf("\nCarte placée: id:%d, x:%d, y:%d, r:%d best:%d\n",readCardId(c),best_x,best_y,best_r,best);
				/*displayList(l);
				afficherCarte(m);*/
				fflag = 0;
				best = -1;
				c = popFirstCard(l);
			}
			else{
				appendHeadList(l,c);
				return best;
			}
		}
	}
	return best;
}

void randomSuffleMap(Map map) {
  int p = 0;

  Card c = NULL;

  List l = createList();

  int n = getListSize(getMapList(map));

  p = rand() % (n - 1) + 1;

  int it = 1;
printf("--Debug randomSuffleMap--\n");
displayList(getMapList(map));
afficherCarte(map);
  while (it < n) {
    appendHeadList(l, retirerCard(map));

    it++;
  }
displayList(l);

  while (readFirst(l) != NULL) {
    c = popFirstCard(l);
    do {
      p = rand() % 5;
      if (p < 2)
        changeRotation(c, (Rotation) rand() % 4);
      else
        setPos(c, rand() % (getSize(map)), rand() % (getSize(map)));

    }while(!respecteRegles(map, c));

    insertCard(map, c, readPos(c).x, readPos(c).y);
    appendHeadList(getMapList(map), c);/*refreshGrid(map);displayList(getMapList(map));afficherCarte(map);*/
  }


  freeList(l);
}

List copyListPack(List b, List l){
	b = createList();

  Element e = readFirst(l);
	Card c = NULL;
	while(e != NULL){
		c = readCardE(e);
  	appendHeadList(b, copyCardM(c));
    e = skipCard(e);
	}

  List a = NULL;
  a = copyList(a, b);

  freeList(b);

  b = a;

	return b;
}

int solveur_recuit(Map map, List l, Point_S ps) {
  srand(time(NULL));
  int best = 0;
  float theta = 0.95;
  float lambda = 0.9999;

  int max_it = 1000;
  int k = 1;

  /*On commence avec une solution "assez" bonne*/
  solveur_distance(map, l, ps);

  /*Suite de solution*/
  List x = NULL;
  x = copyListPack(x, getMapList(map));

  /*Solution optimale*/
  List x_star = NULL;
  x_star = copyListPack(x_star, x);

  /*Solution temporaire*/
  List y = createList();

  List x_tmp = NULL;

  double diff = 0.0;
  double p = 0.0;
  double expo = 0.0;

  while (k < max_it) {
    /*refreshGrid(map);displayList(getMapList(map));afficherCarte(map);*/
    randomSuffleMap(map);

    freeList(y);
    y = NULL;
    y = copyListPack(y, getMapList(map));

    /*
    displayList(getMapList(map));
    displayList(x);
    displayList(x_star);
    displayList(y);
    printf("%d\n", readCardId(readCardE(readFirst(y))));
     exit(1);
     */

    diff = (double) countPoints(map, ps);

    /*appendHeadListE(getMapList(map), readFirst(x));*/
    refreshGrid(map);

    diff -= (double) countPoints(map, ps);
    expo = exp(-diff / theta);


    p = (expo > 1.0) ? 1.0 : expo;

    if (p > (rand() / RAND_MAX)) {
      freeList(x);
      x = copyListPack(x, y);
    }
    /*appendHeadListE(getMapList(map), readFirst(x_star));*/
    refreshGrid(map);

    diff = countPoints(map, ps);

    x_tmp = copyListPack(x_tmp, x);
    /*appendHeadListE(getMapList(map), readFirst(x_tmp));*/
    refreshGrid(map);
    freeList(x_tmp);

    if (diff < countPoints(map, ps)) {
      freeList(x_star);
      x_star = copyListPack(x_star, x);
    }

    k++;
    theta *= lambda;
    /*Pour voir les étapes
    system("clear");*/
    /*displayList(getMapList(map));*/

    afficherCarte(map);
  }
  freeList(y);
  freeList(x);
  /*appendHeadListE(getMapList(map), readFirst(x_star));*/
  freeList(x_star);
  refreshGrid(map);
  afficherCarte(map);
	
  return best;
}

/**
* \file listlayer.h
*
* Structures List et Card utilisés par list.h et layer.h
*/
#ifndef LISTLAYER_H
#define LISTLAYER_H

/**
* \struct Card
* \brief Objet Card.
*
* Card est un objet qui permet de gérer les tuiles
* à l'aide des fonctions de la librairie layer.h
*/
enum Rotation { TOP, BOTTOM, LEFT, RIGHT };
enum Ground { VIDE, L, V, F, R, U, P };
typedef enum Ground Ground;
typedef enum Rotation Rotation;

typedef struct Position{
	int x;
	int y;	
} Position;

/**
 * \internal Une tuile composée de 6 cases de type Ground
 */
struct Card_s {
    Ground cases[6];/* Une tuile de base */
    Rotation rot;
    Position pos;
    int id; /* IMPORTANT : ID positive ou null */
};
struct Card_s;
typedef struct Card_s* Card;

typedef struct Element_s* Element;
typedef struct List_s* List;

#endif


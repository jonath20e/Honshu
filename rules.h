/**
* \file rules.h
*
* Toutes les fonctions qui servent à gérer les 
* règles  du jeu
*/
#ifndef RULES_H
#define RULES_H

/** 
 * \brief test tous les règles
 */
int respecteRegles(Map m, Card c);

/**
* \brief Recouvrement partiel obligatoire
* \param Map
* \param Card
* \return 1 si le recouvrement est respecté, 0 sinon
*/
int coverTest(Map m, Card c);


/**
* \brief Vérifie si un la carte va recouvrir un lac
* \param Map
* \param Card
* \return 1 si un lac s'y trouve, 0 sinon
*/
int lacTest(Map map, Card card);


/**
* \brief Vérifie si la carte dépasse la limitation de la zone autorisée par la grille
* \param Map
* \param Card
* \return 1 si la carte dépasse la zone autorisée, 0 sinon
*/
int limitArea(Map map, Card card);

int groundTest(Map map, Card card, Ground g);



#endif

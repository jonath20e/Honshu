/**
* \file layer.h
*
* Toutes les fonctions qui servent à gérer les tuiles
*/
#ifndef LAYER_H
#define LAYER_H

#include "listlayer.h"

#define RED "\x1B[31m"
#define GRN "\x1B[32m"
#define YEL "\x1B[33m"
#define BLU "\x1B[34m"
#define MAG "\x1B[35m"
#define CYN "\x1B[36m"
#define WHT "\x1B[37m"
#define RESET "\x1B[0m"

/**
 * \brief Crée une tuile alétoirement
 * \return Renvoie une tuile aléatoire de type Card, avec
 * une rotation TOP par défaut
 * \attention Il faut impérativement appeler la fonction srand() de la
 * librairie time.h pour pouvoir utiliser cette fonction
 * \internal crée un objet Card
 */
Card initCard();

Card copyCard(Card c);

Card copyCardM(Card c);
/**
 * \brief Crée une liste de tuiles à partir d'un fichier
 * \detail Format d'un ensemble de tuiles de type
 * count
 * id_1
 * X_1 X_2
 * X_3 X_4
 * X_5 X_6
 * id_2 ...
 * avec count le nombre de tuiles a initialisée (int)
 * avec id_1 l'identité de la tuile
 * avec X_M le terrain de la M-ième case égale à l'une des
 * valeurs ci-contre { L, V, F, R, U, C } (char)
 * \param file une chaîne de caractère qui contient le chemin du fichier
 * \param l une variable de type List
 * \attention count doit être inférieure à 100 tout comme les ids
 * \return Renvoie une liste donnée par un fichier d'éléments de type Card
 * NULL si la variable file est incorrecte ou le format de fichier
 * ne correspond pas au norme
 * \internal crée un objet Card en utilisant les fonctions du noyau
 */
List initCardFile(char *file, List pack);

/*
 * \brief Crée une liste de cartes aléatoirement
 * \param taille la taille de la grille
 * \return une liste crée aléatoirement
 */
List initCardPack(int taille);

void cardCopy(Card card, const Card c_copy);
/**
 * \brief Détruit la tuile de type Card
 * \param card une variable de type Card
 * \internal On libère la mémoire qui a été allouée pour créer la tuile
 */
void freeCard(Card card);

/**
 * \brief Lit la rotation actuelle de la tuile
 * \param card une variable de type Card
 * \return renvoie la rotation de la tuile
 */
Rotation readRot(Card card);

/**
 * \brief Lit l'identifiant de la tuile
 * \param card une variable de type Card
 * \return renvoie l'id de la tuile
 */
int readCardId(Card card);

/**
 * \brief Lit un état de la tuile à une position donnée
 * \param card une variable de type Card
 * \param x un entier
 * \renvoie un état de la tuile
 */
Ground readCard(Card card, int x);

/**
 * \brief Lit un état de la tuile à une position donnée
 * \param card une variable de type Card
 * \param x un entier
 * \param y un entier
 * \renvoie un état de la tuile
 */
Ground readCardM(Card card, int x, int y);

/**
* \brief Lit la position actuelle de la tuile
* \param card une variable de type Card
* \return renvoie la position de la tuile
*/
struct Position readPos(Card card);


/**
* \brief Change l'identifiant de la tuile card par la valeur cardid
* \param card une variable de type Card
* \param cardid une variable de type Card
*/
void setId(Card card, int cardid);

/**
 * \brief Change la position de la tuile card
 * \param card une variable de type Card
 * \param x une variable de type int
 * \param y une variable de type int
 */
void setPos(Card card, int x, int y);

void afficherCase(int t);

/**
 * \brief Change la rotation de la tuile card
 * \param card une variable de type Card
 * \param rot une variable de type Rotation
 */
void changeRotation(Card card, Rotation rot);

/**
 * \brief affiche une carte en console
 * \param une structure de type carte
 * */
void displayCard(Card card);

/**
 * \brief affiche une liste de cartes en console
 * \param une structure de type List
 * */
void displayPack(List pack);

/**
 * \brief insère un type de terrain sur un des cases de la carte
 * \param une structure de type Card
 * \param le char représentant le type de la charte
 * \param le coordonné x du terrain
 * \param le coordonné y du terrain
 * */
int setGround(Card card, char cardtype, int x, int y);


#endif
